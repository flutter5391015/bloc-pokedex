import 'package:flutter/material.dart';

const defaultTypeColor = Color(0xff333333);
const waterTypeColor = Color(0xff5090D6);
const dragonTypeColor = Color(0xff0B6DC3);
const electricTypeColor = Color(0xffF4D23C);
const palaceTypeColor = Color(0xffEC8FE6);
const ghostTypeColor = Color(0xff5269AD);
const fighterTypeColor = Color(0xffCE416B);
const normalTypeColor = Color(0xff919AA2);
const nocturnalTypeColor = Color(0xff5A5465);
const metalTypeColor = Color(0xff5A8EA2);
const stoneTypeColor = Color(0xffC5B78C);
const psychicTypeColor = Color(0xffFA7179);
const landTypeColor = Color(0xffD97845);
const poisonousTypeColor = Color(0xffB567CE);
const flyingTypeColor = Color(0xff89AAE3);

const primaryColor = Color(0xff173EA5);
const disabledColor = Color(0xffE6E6E6);
const whiteTextColor = Color(0xffFFFFFF);
const blackTextColor = Color(0xff000000);
const disabledTextColor = Color(0xff999999);
const errorColor = Color(0xffCD3131);
const grayColor = Color(0xff666666);
const grayDarkerColor = Color(0xff4D4D4D);
