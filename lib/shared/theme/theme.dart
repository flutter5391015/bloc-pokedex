import 'package:bloc_pokedex_app/shared/theme/theme_config.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:google_fonts/google_fonts.dart';

ThemeData getDefaultTheme() {
  return ThemeData().copyWith(
    primaryColor: blackTextColor,
    scaffoldBackgroundColor: Colors.white,
    appBarTheme: AppBarTheme(
      backgroundColor: Colors.white,
      elevation: 0,
      centerTitle: true,
      titleTextStyle: GoogleFonts.poppins(
        color: blackTextColor,
        fontSize: 18.sp,
        fontStyle: FontStyle.normal,
        fontWeight: FontWeight.normal,
      ),
      iconTheme: const IconThemeData(
        color: blackTextColor,
      ),
    ),
    textTheme: TextTheme(
      titleSmall: GoogleFonts.poppins(
        fontSize: 12.sp,
        color: blackTextColor,
      ),
      titleMedium: GoogleFonts.poppins(
        fontSize: 16.sp,
        color: blackTextColor,
      ),
      titleLarge: GoogleFonts.poppins(
        fontSize: 26.sp,
        color: blackTextColor,
        fontWeight: FontWeight.normal,
      ),
      displayLarge: GoogleFonts.poppins(
        fontSize: 14.sp,
        fontWeight: FontWeight.normal,
        color: blackTextColor,
      ),
      bodyMedium: GoogleFonts.poppins(
        fontSize: 16.sp,
        fontWeight: FontWeight.normal,
        color: blackTextColor,
      ),
      bodySmall: GoogleFonts.poppins(
        fontSize: 14.sp,
        fontWeight: FontWeight.normal,
        color: blackTextColor,
      ),
    ),
    inputDecorationTheme: InputDecorationTheme(
      hoverColor: Colors.transparent,
      border: OutlineInputBorder(
        borderRadius: BorderRadius.circular(8.0),
        borderSide: BorderSide(
          color: Colors.grey[300]!,
        ),
      ),
      enabledBorder: OutlineInputBorder(
        borderRadius: BorderRadius.circular(8.0),
        borderSide: const BorderSide(
          color: Colors.transparent,
        ),
      ),
      errorBorder: OutlineInputBorder(
        borderRadius: BorderRadius.circular(8.0),
        borderSide: const BorderSide(
          color: errorColor,
        ),
      ),
      disabledBorder: OutlineInputBorder(
        borderRadius: BorderRadius.circular(8.0),
        borderSide: BorderSide(
          color: Colors.grey[300]!,
        ),
      ),
      focusedBorder: OutlineInputBorder(
        borderRadius: BorderRadius.circular(8.0),
        borderSide: const BorderSide(
          color: primaryColor,
        ),
      ),
      filled: true,
    ),
  );
}
