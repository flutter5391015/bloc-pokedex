import 'package:bloc_pokedex_app/core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class PKText extends StatelessWidget {
  final String label;
  final Color? color;
  final FontWeight? fontWeight;
  final double? fontSize;
  final double? width;
  final TextAlign? textAlign;

  const PKText({
    super.key,
    required this.label,
    this.color,
    this.fontWeight,
    this.fontSize,
    this.width,
    this.textAlign,
  });

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: width,
      child: Text(
        label,
        textAlign: textAlign,
        style: Theme.of(context).textTheme.titleLarge!.copyWith(
              fontSize: fontSize != null ? fontSize!.sp : 26.0.sp,
              fontWeight: fontWeight ?? FontWeight.w600,
              color: color ?? blackTextColor,
              fontStyle: FontStyle.normal,
            ),
      ),
    );
  }
}
