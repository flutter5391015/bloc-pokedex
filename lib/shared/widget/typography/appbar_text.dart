import 'package:bloc_pokedex_app/core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class AppBarText extends StatelessWidget {
  final String label;
  final FontWeight? fontWeight;
  final double? fontSize;

  const AppBarText({
    super.key,
    required this.label,
    this.fontWeight,
    this.fontSize,
  });

  @override
  Widget build(BuildContext context) {
    return Text(
      label,
      style: Theme.of(context).textTheme.labelMedium!.copyWith(
            color: blackTextColor,
            fontWeight: fontWeight ?? FontWeight.w600,
            fontSize: fontSize ?? 18.sp,
          ),
    );
  }
}
