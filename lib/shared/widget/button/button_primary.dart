import 'package:bloc_pokedex_app/shared/theme/theme_config.dart';
import 'package:bloc_pokedex_app/shared/widget/typography/pk_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class ButtonPrimary extends StatelessWidget {
  final String label;
  final Function onPressed;
  final bool isDisabled;

  const ButtonPrimary({
    super.key,
    required this.label,
    required this.onPressed,
    this.isDisabled = false,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      height: 50.h,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(50).r,
        color: isDisabled ? disabledColor : primaryColor,
      ),
      child: ElevatedButton(
        style: ElevatedButton.styleFrom(
          backgroundColor: Colors.transparent,
          elevation: 0,
          shadowColor: Colors.transparent,
          disabledBackgroundColor: Colors.transparent,
          disabledForegroundColor: Colors.transparent,
        ),
        onPressed: isDisabled ? null : () => onPressed(),
        child: PKText(
          label: label,
          fontSize: 18,
          fontWeight: FontWeight.w600,
          color: isDisabled ? disabledTextColor : whiteTextColor,
        ),
      ),
    );
  }
}
