import 'package:bloc_pokedex_app/shared/theme/theme_config.dart';
import 'package:bloc_pokedex_app/shared/widget/typography/pk_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';

class ButtonOutlineIcon extends StatelessWidget {
  final String label;
  final String icon;
  final Function onPressed;

  const ButtonOutlineIcon({
    super.key,
    required this.label,
    required this.icon,
    required this.onPressed,
  });

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: MediaQuery.of(context).size.width,
      height: 50.h,
      child: OutlinedButton(
        style: OutlinedButton.styleFrom(
          foregroundColor: primaryColor.withOpacity(0.9),
          side: const BorderSide(
            color: primaryColor,
            width: 1.0,
          ),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(50.0), // Border radius
          ),
        ),
        onPressed: () => onPressed(),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            SizedBox(
              width: 21.w,
              height: 26.w,
              child: SvgPicture.asset(
                icon,
              ),
            ),
            SizedBox(
              width: 12.0.w,
            ),
            PKText(
              label: label,
              fontSize: 16,
              color: primaryColor,
            )
          ],
        ),
      ),
    );
  }
}
