import 'package:bloc_pokedex_app/core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class PKPasswordTextField extends StatefulWidget {
  final String? id;
  final String? label;
  final String? value;
  final String? hintText;
  final bool? isEnabled;
  final FocusNode focusNode;
  final String? Function(String?)? validator;
  final Function(String) onChanged;

  const PKPasswordTextField({
    Key? key,
    this.id,
    this.label,
    this.isEnabled,
    this.value,
    this.hintText,
    this.validator,
    required this.onChanged,
    required this.focusNode,
  }) : super(key: key);

  @override
  State<PKPasswordTextField> createState() => _QTextFieldState();
}

class _QTextFieldState extends State<PKPasswordTextField> {
  TextEditingController textEditingController = TextEditingController();
  bool isObscure = true;
  bool isFocused = false;

  @override
  void initState() {
    textEditingController.text = widget.value ?? "";
    widget.focusNode.addListener(_onFocusChange);
    super.initState();
  }

  void _onFocusChange() {
    isFocused = widget.focusNode.hasFocus;
    setState(() {});
  }

  getValue() {
    return textEditingController.text;
  }

  setValue(value) {
    textEditingController.text = value;
  }

  resetValue() {
    textEditingController.text = "";
  }

  @override
  void dispose() {
    // TODO: implement dispose
    widget.focusNode.removeListener(_onFocusChange);
    textEditingController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisAlignment: MainAxisAlignment.start,
      mainAxisSize: MainAxisSize.min,
      children: [
        if (widget.label != null) ...[
          PKText(
            label: widget.label!,
            textAlign: TextAlign.start,
            fontSize: 14,
            fontWeight: FontWeight.w600,
          ),
          SizedBox(
            height: 3.0.h,
          ),
        ],
        TextFormField(
          enabled: widget.isEnabled ?? true,
          style: const TextStyle(
            color: primaryColor,
          ),
          scrollPadding:
              EdgeInsets.only(bottom: MediaQuery.of(context).viewInsets.bottom),
          controller: textEditingController,
          focusNode: widget.focusNode,
          autocorrect: false,
          cursorColor: primaryColor,
          validator: widget.validator,
          obscureText: isObscure,
          keyboardType: TextInputType.text,
          decoration: InputDecoration(
            suffixIcon: GestureDetector(
              onTap: () {
                setState(() {
                  isObscure = !isObscure;
                });
              },
              child: Icon(
                isObscure ? Icons.visibility_off : Icons.visibility,
              ),
            ),
            suffixIconColor: isFocused
                ? primaryColor
                : const Color(0xff999999).withOpacity(0.3),
            filled: true,
            fillColor: whiteTextColor,
            contentPadding:
                const EdgeInsets.symmetric(vertical: 16.0, horizontal: 14).w,
            enabledBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(5.0).r,
              borderSide: BorderSide(
                color: const Color(0xff999999).withOpacity(0.3),
              ),
            ),
            focusedBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(5.0).r,
              borderSide: const BorderSide(
                color: Color(0xff999999),
              ),
            ),
            hintText: widget.hintText,
            hintStyle: Theme.of(context).textTheme.labelSmall!.copyWith(
                  fontSize: 16,
                  fontWeight: FontWeight.w400,
                  color: const Color(0xff999999),
                  letterSpacing: 0,
                ),
          ),
          onChanged: (value) {
            widget.onChanged(value);
          },
        ),
      ],
    );
  }
}
