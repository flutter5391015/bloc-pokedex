import 'package:bloc_pokedex_app/core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class PKCommonTextField extends StatefulWidget {
  final String? id;
  final bool? isEnabled;
  final String? value;
  final String? hintText;
  final FocusNode focusNode;
  final String? Function(String?)? validator;
  final Function(String) onChanged;

  const PKCommonTextField({
    Key? key,
    this.id,
    this.isEnabled,
    this.value,
    this.hintText,
    this.validator,
    required this.onChanged,
    required this.focusNode,
  }) : super(key: key);

  @override
  State<PKCommonTextField> createState() => _QTextFieldState();
}

class _QTextFieldState extends State<PKCommonTextField> {
  TextEditingController textEditingController = TextEditingController();

  @override
  void initState() {
    textEditingController.text = widget.value ?? "";
    super.initState();
  }

  getValue() {
    return textEditingController.text;
  }

  setValue(value) {
    textEditingController.text = value;
  }

  resetValue() {
    textEditingController.text = "";
  }

  @override
  void dispose() {
    // TODO: implement dispose
    textEditingController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      enabled: widget.isEnabled ?? true,
      autocorrect: false,
      style: const TextStyle(
        color: primaryColor,
      ),
      cursorColor: primaryColor,
      controller: textEditingController,
      focusNode: widget.focusNode,
      validator: widget.validator,
      decoration: InputDecoration(
        filled: true,
        fillColor: whiteTextColor,
        contentPadding:
            const EdgeInsets.symmetric(vertical: 16.0, horizontal: 14).w,
        enabledBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(5.0).r,
          borderSide: BorderSide(
            color: const Color(0xff999999).withOpacity(0.3),
          ),
        ),
        focusedBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(5.0).r,
          borderSide: const BorderSide(
            color: Color(0xff999999),
          ),
        ),
        hintText: widget.hintText,
        hintStyle: Theme.of(context).textTheme.labelSmall!.copyWith(
              fontSize: 16,
              fontWeight: FontWeight.w400,
              color: const Color(0xff999999),
              letterSpacing: 0,
            ),
      ),
      onChanged: (value) {
        widget.onChanged(value);
      },
    );
  }
}
