import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'dart:math' as math;

class LoadingScreenView extends StatefulWidget {
  const LoadingScreenView({Key? key}) : super(key: key);

  @override
  State<LoadingScreenView> createState() => _LoadingScreenViewState();
}

class _LoadingScreenViewState extends State<LoadingScreenView>
    with SingleTickerProviderStateMixin {
  late AnimationController _animationController;
  late Animation<double> _animation;

  @override
  void initState() {
    _animationController = AnimationController(
      duration: const Duration(milliseconds: 500),
      vsync: this,
    )..repeat();

    super.initState();
  }

  @override
  void dispose() {
    _animationController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color(0xff000029),
      body: Center(
        child: SizedBox(
          width: 48.0.w,
          height: 48.0.h,
          child: AnimatedBuilder(
            animation: _animationController,
            builder: (_, child) {
              return Transform.rotate(
                angle: _animationController.value * 2 * math.pi,
                child: child,
              );
            },
            child: Image.asset(
              "assets/images/loading.png",
              fit: BoxFit.fill,
            ),
          ),
        ),
      ),
    );
  }
}
