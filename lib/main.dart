import 'package:bloc_pokedex_app/core.dart';
import 'package:bloc_pokedex_app/module/login/view/login_view.dart';
import 'package:bloc_pokedex_app/module/onboarding/view/onboarding_view.dart';
import 'package:bloc_pokedex_app/shared/theme/theme.dart';
import 'package:bloc_pokedex_app/state_util.dart';
import 'package:flutter/material.dart';
import 'package:flutter_native_splash/flutter_native_splash.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:shared_preferences/shared_preferences.dart';

void main() async {
  WidgetsBinding widgetsBinding = WidgetsFlutterBinding.ensureInitialized();
  FlutterNativeSplash.preserve(widgetsBinding: widgetsBinding);
  SharedPreferences pref = await SharedPreferences.getInstance();
  bool isAlreadyVisited = pref.getBool('alreadyVisited') ?? false;
  FlutterNativeSplash.remove();

  runApp(MyApp(
    isAlreadyVisited: isAlreadyVisited,
  ));
}

class MyApp extends StatelessWidget {
  final bool isAlreadyVisited;
  const MyApp({
    super.key,
    required this.isAlreadyVisited,
  });

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return ScreenUtilInit(
      designSize: const Size(360, 800),
      minTextAdapt: true,
      splitScreenMode: true,
      builder: (context, child) {
        return MaterialApp(
          debugShowCheckedModeBanner: false,
          navigatorKey: Get.navigatorKey,
          theme: getDefaultTheme(),
          home: child,
        );
      },
      child: isAlreadyVisited ? const HomeView() : const OnboardingView(),
    );
  }
}
