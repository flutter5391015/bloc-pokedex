import 'package:bloc_pokedex_app/shared/theme/theme_config.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class LoginSkipButtonWidget extends StatelessWidget {
  final Function onPressed;
  const LoginSkipButtonWidget({
    super.key,
    required this.onPressed,
  });

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => onPressed(),
      child: Row(
        children: [
          Expanded(
            child: Container(
              alignment: Alignment.centerRight,
              child: Text(
                "Skip",
                style: Theme.of(context).textTheme.labelMedium!.copyWith(
                      fontSize: 18.sp,
                      color: blackTextColor,
                      fontWeight: FontWeight.w500,
                    ),
              ),
            ),
          ),
          SizedBox(
            width: 6.0.w,
          ),
          Icon(
            Icons.arrow_forward,
            size: 27.h,
          ),
        ],
      ),
    );
  }
}
