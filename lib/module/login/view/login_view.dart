import 'package:bloc_pokedex_app/core.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_native_splash/flutter_native_splash.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class LoginView extends StatefulWidget {
  const LoginView({Key? key}) : super(key: key);

  @override
  State<LoginView> createState() => _LoginViewState();
}

class _LoginViewState extends State<LoginView> {
  LoginBloc bloc = LoginBloc();

  @override
  void initState() {
    bloc.initState();
    FlutterNativeSplash.remove();
    _hideKeyboard();
    super.initState();
  }

  void _hideKeyboard() {
    SystemChannels.textInput.invokeMethod('TextInput.hide');
  }

  @override
  void dispose() {
    bloc.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (BuildContext context) => bloc,
      child: BlocListener<LoginBloc, LoginState>(
        listener: (context, state) {},
        child: BlocBuilder<LoginBloc, LoginState>(
          builder: (context, state) {
            final bloc = context.read<LoginBloc>();
            return buildView(context, bloc, state);
          },
        ),
      ),
    );
  }

  buildView(
    BuildContext context,
    LoginBloc bloc,
    LoginState state,
  ) {
    return Scaffold(
      body: Container(
        padding: const EdgeInsets.only(
          left: 16,
          right: 16,
          top: 40,
          bottom: 40,
        ).w,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Expanded(
              child: Column(
                children: [
                  LoginSkipButtonWidget(onPressed: () {
                    Navigator.pushReplacement(
                      context,
                      MaterialPageRoute(
                        builder: (context) => const HomeView(),
                      ),
                    );
                  }),
                  const Spacer(),
                  Image.asset(
                    'assets/images/login.png',
                  ),
                  SizedBox(
                    height: 48.0.h,
                  ),
                  const PKText(
                    label: 'Are you ready for this adventure?',
                    fontSize: 26,
                    fontWeight: FontWeight.w500,
                    textAlign: TextAlign.center,
                  ),
                  SizedBox(
                    height: 16.0.h,
                  ),
                  const PKText(
                    label:
                        'Just create an account and start exploring the world of Pokémon today!',
                    fontSize: 14,
                    fontWeight: FontWeight.w400,
                    color: grayColor,
                    textAlign: TextAlign.center,
                  ),
                  SizedBox(
                    height: 32.0.h,
                  ),
                ],
              ),
            ),
            ButtonPrimary(
              label: 'Create an account',
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => const RegisterView(),
                  ),
                );
              },
            ),
            SizedBox(
              height: 16.h,
            ),
            ButtonText(
                label: 'I already have an account',
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => const RegisteredView(),
                    ),
                  );
                }),
          ],
        ),
      ),
    );
  }
}
