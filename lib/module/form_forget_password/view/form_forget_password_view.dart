import 'package:bloc_pokedex_app/core.dart';
import 'package:bloc_pokedex_app/module/form_forget_password/widget/form_forget_password_email_widget.dart';
import 'package:bloc_pokedex_app/module/form_forget_password/widget/form_forget_password_otp_widget.dart';
import 'package:bloc_pokedex_app/module/form_forget_password/widget/form_forget_password_password_widget.dart';
import 'package:bloc_pokedex_app/utils/focus_helper.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class FormForgetPasswordView extends StatefulWidget {
  const FormForgetPasswordView({Key? key}) : super(key: key);

  @override
  State<FormForgetPasswordView> createState() => _FormForgetPasswordViewState();
}

class _FormForgetPasswordViewState extends State<FormForgetPasswordView> {
  late PageController _pageController;
  late FocusNode _emailFocusNode;
  late FocusNode _passwordFocusNode;
  final List<FocusNode> _otpFocusNode = [];
  FormForgetPasswordBloc bloc = FormForgetPasswordBloc();

  @override
  void initState() {
    bloc.initState();
    _pageController = PageController(
      initialPage: 0,
    );
    _emailFocusNode = FocusNode();
    _passwordFocusNode = FocusNode();
    for (int i = 0; i < 6; i++) {
      _otpFocusNode.add(FocusNode());
    }
    super.initState();
  }

  @override
  void dispose() {
    bloc.dispose();
    _emailFocusNode.dispose();
    _passwordFocusNode.dispose();
    for (int i = 0; i < 6; i++) {
      _otpFocusNode[i].dispose();
    }
    _pageController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (BuildContext context) => bloc,
      child: BlocListener<FormForgetPasswordBloc, FormForgetPasswordState>(
        listenWhen: (previous, current) {
          if (current.isOTPSended == true && previous.selectedIndex == 0) {
            _pageController.nextPage(
              duration: const Duration(milliseconds: 150),
              curve: Curves.bounceIn,
            );

            return true;
          }
          if (current.isOtpValidated == true && previous.selectedIndex == 1) {
            _pageController.nextPage(
              duration: const Duration(milliseconds: 150),
              curve: Curves.bounceIn,
            );

            return true;
          }
          if (current.isCompleted == true && previous.selectedIndex == 2) {
            Navigator.pushAndRemoveUntil(
              context,
              MaterialPageRoute(
                builder: (context) => const LoginSuccessView(),
              ),
              (Route<dynamic> route) => false,
            );

            return true;
          }
          return false;
        },
        listener: (context, state) {},
        child: BlocBuilder<FormForgetPasswordBloc, FormForgetPasswordState>(
          builder: (context, state) {
            if (state.isLoadingCreateNewPassword) {
              return const LoadingScreenView();
            }
            final bloc = context.read<FormForgetPasswordBloc>();
            return buildView(context, bloc, state);
          },
        ),
      ),
    );
  }

  buildView(
    BuildContext context,
    FormForgetPasswordBloc bloc,
    FormForgetPasswordState state,
  ) {
    return Scaffold(
      appBar: AppBar(
        title: const AppBarText(label: 'I forgot my password'),
      ),
      body: GestureDetector(
        onTap: () => FocusHelper.unfocus(context),
        child: Container(
          padding: const EdgeInsets.only(
            bottom: 40,
            left: 16,
            right: 16,
          ).w,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Expanded(
                child: PageView.builder(
                  controller: _pageController,
                  itemCount: 3,
                  itemBuilder: (context, index) {
                    if (index == 0) {
                      return FFPEmailWidget(
                        isEnabled: state.isLoading ? false : true,
                        defaultValue: state.formResetPassword['email'] ?? '',
                        focusNode: _emailFocusNode,
                        onChange: (value) {
                          bloc.add(
                            FormForgetPasswordOnChangeInputFieldEvent(
                              email: value,
                              otp: state.formResetPassword['otp'] ?? '',
                              password:
                                  state.formResetPassword['password'] ?? '',
                            ),
                          );
                        },
                      );
                    } else if (index == 1) {
                      return FFPOtpWidget(
                        defaultValue: state.formResetPassword['otp'] ?? '',
                        focusNode: _otpFocusNode,
                        onChange: (value) {
                          bloc.add(
                            FormForgetPasswordOnChangeInputFieldEvent(
                              email: state.formResetPassword['email'] ?? '',
                              otp: value,
                              password:
                                  state.formResetPassword['password'] ?? '',
                            ),
                          );
                        },
                        isEnabled: state.isLoading ? false : true,
                      );
                    }

                    return FFPPasswordWidget(
                      isEnabled: state.isLoading ? false : true,
                      defaultValue: state.formResetPassword['password'] ?? '',
                      focusNode: _passwordFocusNode,
                      onChange: (value) {
                        bloc.add(
                          FormForgetPasswordOnChangeInputFieldEvent(
                            email: state.formResetPassword['email'] ?? '',
                            otp: state.formResetPassword['otp'] ?? '',
                            password: value,
                          ),
                        );
                      },
                    );
                  },
                ),
              ),
              ButtonPrimary(
                isDisabled: state.isDisabled || state.isLoading,
                label: state.selectedIndex == 2 ? 'Reset' : 'Continue',
                onPressed: () =>
                    state.isDisabled ? null : bloc.onSubmitHandler(),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
