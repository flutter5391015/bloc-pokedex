import 'package:bloc_pokedex_app/utils/validator.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import '../event/form_forget_password_event.dart';
import '../state/form_forget_password_state.dart';

mixin _BlocLifecycle {
  void initState() {}
  void dispose() {}
}

class FormForgetPasswordBloc
    extends Bloc<FormForgetPasswordEvent, FormForgetPasswordState>
    with _BlocLifecycle {
  FormForgetPasswordBloc() : super(FormForgetPasswordState()) {
    on<FormForgetPasswordOnChangedIndexEvent>((event, emit) {
      state.selectedIndex = event.selectedIndex;
      emit(state.copyWith());

      add(FormForgetPasswordOnChangeDisabledEvent());
    });

    on<FormForgetPasswordOnInitEvent>((event, emit) {
      state.selectedIndex = 0;
      state.isLoadingCreateNewPassword = false;
      state.isLoading = false;
      state.isCompleted = false;
      state.isOtpValidated = false;
      state.isOTPSended = false;
      state.isOTPExpired = false;
      state.formResetPassword = {
        "email": "",
        "otp": "",
        "password": "",
      };
      emit(state.copyWith());
    });

    on<FormForgetPasswordOnChangeDisabledEvent>((event, emit) {
      if (state.selectedIndex == 0) {
        if (state.formResetPassword['email'] != '') {
          state.isDisabled = false;
        } else {
          state.isDisabled = true;
        }
      } else if (state.selectedIndex == 1) {
        if (state.formResetPassword['otp'] != null &&
            state.formResetPassword['otp'] != '' &&
            state.formResetPassword['otp']!.length == 6) {
          state.isDisabled = false;
        } else {
          state.isDisabled = true;
        }
      } else {
        if (state.formResetPassword['password'] != null &&
            state.formResetPassword['password'] != '' &&
            state.formResetPassword['password']!.length >= 8) {
          state.isDisabled = false;
        } else {
          state.isDisabled = true;
        }
      }
      emit(state.copyWith());
    });

    on<FormForgetPasswordOnChangeInputFieldEvent>((event, emit) {
      var hasRightFormatEmail = Validator.email(event.email);

      state.formResetPassword = {
        "email": hasRightFormatEmail == null ? event.email : '',
        "otp": event.otp,
        "password": event.password,
      };
      emit(state.copyWith());
      add(FormForgetPasswordOnChangeDisabledEvent());
    });

    on<FormForgetPasswordOnValidateOTPEvent>((event, emit) async {
      state.isLoading = true;
      state.isOtpValidated = false;
      emit(state.copyWith());

      await Future.delayed(
        const Duration(seconds: 3),
      );
      state.isLoading = false;
      state.isOtpValidated = true;
      emit(state.copyWith());

      add(
        FormForgetPasswordOnChangedIndexEvent(selectedIndex: 2),
      );
    });

    on<FormForgetPasswordOnSubmitNewPasswordEvent>((event, emit) async {
      state.isLoading = true;
      state.isLoadingCreateNewPassword = true;
      state.isCompleted = false;
      emit(state.copyWith());

      await Future.delayed(
        const Duration(seconds: 3),
      );
      state.isLoading = false;
      state.isCompleted = true;
      state.isLoadingCreateNewPassword = false;
      emit(state.copyWith());
    });

    on<FormForgetPasswordOnSendOTPEvent>((event, emit) async {
      state.isLoading = true;
      state.isOTPSended = false;
      emit(state.copyWith());

      await Future.delayed(
        const Duration(seconds: 3),
      );

      state.isLoading = false;
      state.isOTPSended = true;

      emit(state.copyWith());

      add(
        FormForgetPasswordOnChangedIndexEvent(selectedIndex: 1),
      );
    });

    initState();
  }

  @override
  void initState() {
    //initState event
    super.initState();
    add(FormForgetPasswordOnInitEvent());
  }

  @override
  void dispose() {
    //dispose event
    super.dispose();
  }

  @override
  Future<void> close() {
    dispose();
    return super.close();
  }

  void onSubmitHandler() {
    if (state.selectedIndex == 0) {
      add(FormForgetPasswordOnSendOTPEvent());
    } else if (state.selectedIndex == 1) {
      add(FormForgetPasswordOnValidateOTPEvent());
    } else {
      add(FormForgetPasswordOnSubmitNewPasswordEvent());
    }
  }
}
