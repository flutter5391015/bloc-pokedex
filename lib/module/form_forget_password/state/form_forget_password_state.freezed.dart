// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'form_forget_password_state.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$FormForgetPasswordState {
  int get selectedIndex => throw _privateConstructorUsedError;
  set selectedIndex(int value) => throw _privateConstructorUsedError;
  bool get isLoading => throw _privateConstructorUsedError;
  set isLoading(bool value) => throw _privateConstructorUsedError;
  bool get isLoadingCreateNewPassword => throw _privateConstructorUsedError;
  set isLoadingCreateNewPassword(bool value) =>
      throw _privateConstructorUsedError;
  bool get isDisabled => throw _privateConstructorUsedError;
  set isDisabled(bool value) => throw _privateConstructorUsedError;
  bool get isOTPSended => throw _privateConstructorUsedError;
  set isOTPSended(bool value) => throw _privateConstructorUsedError;
  bool get isOtpValidated => throw _privateConstructorUsedError;
  set isOtpValidated(bool value) => throw _privateConstructorUsedError;
  bool get isOTPExpired => throw _privateConstructorUsedError;
  set isOTPExpired(bool value) => throw _privateConstructorUsedError;
  bool get isCompleted => throw _privateConstructorUsedError;
  set isCompleted(bool value) => throw _privateConstructorUsedError;
  Map<String, String> get formResetPassword =>
      throw _privateConstructorUsedError;
  set formResetPassword(Map<String, String> value) =>
      throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $FormForgetPasswordStateCopyWith<FormForgetPasswordState> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $FormForgetPasswordStateCopyWith<$Res> {
  factory $FormForgetPasswordStateCopyWith(FormForgetPasswordState value,
          $Res Function(FormForgetPasswordState) then) =
      _$FormForgetPasswordStateCopyWithImpl<$Res, FormForgetPasswordState>;
  @useResult
  $Res call(
      {int selectedIndex,
      bool isLoading,
      bool isLoadingCreateNewPassword,
      bool isDisabled,
      bool isOTPSended,
      bool isOtpValidated,
      bool isOTPExpired,
      bool isCompleted,
      Map<String, String> formResetPassword});
}

/// @nodoc
class _$FormForgetPasswordStateCopyWithImpl<$Res,
        $Val extends FormForgetPasswordState>
    implements $FormForgetPasswordStateCopyWith<$Res> {
  _$FormForgetPasswordStateCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? selectedIndex = null,
    Object? isLoading = null,
    Object? isLoadingCreateNewPassword = null,
    Object? isDisabled = null,
    Object? isOTPSended = null,
    Object? isOtpValidated = null,
    Object? isOTPExpired = null,
    Object? isCompleted = null,
    Object? formResetPassword = null,
  }) {
    return _then(_value.copyWith(
      selectedIndex: null == selectedIndex
          ? _value.selectedIndex
          : selectedIndex // ignore: cast_nullable_to_non_nullable
              as int,
      isLoading: null == isLoading
          ? _value.isLoading
          : isLoading // ignore: cast_nullable_to_non_nullable
              as bool,
      isLoadingCreateNewPassword: null == isLoadingCreateNewPassword
          ? _value.isLoadingCreateNewPassword
          : isLoadingCreateNewPassword // ignore: cast_nullable_to_non_nullable
              as bool,
      isDisabled: null == isDisabled
          ? _value.isDisabled
          : isDisabled // ignore: cast_nullable_to_non_nullable
              as bool,
      isOTPSended: null == isOTPSended
          ? _value.isOTPSended
          : isOTPSended // ignore: cast_nullable_to_non_nullable
              as bool,
      isOtpValidated: null == isOtpValidated
          ? _value.isOtpValidated
          : isOtpValidated // ignore: cast_nullable_to_non_nullable
              as bool,
      isOTPExpired: null == isOTPExpired
          ? _value.isOTPExpired
          : isOTPExpired // ignore: cast_nullable_to_non_nullable
              as bool,
      isCompleted: null == isCompleted
          ? _value.isCompleted
          : isCompleted // ignore: cast_nullable_to_non_nullable
              as bool,
      formResetPassword: null == formResetPassword
          ? _value.formResetPassword
          : formResetPassword // ignore: cast_nullable_to_non_nullable
              as Map<String, String>,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$_FormForgetPasswordStateCopyWith<$Res>
    implements $FormForgetPasswordStateCopyWith<$Res> {
  factory _$$_FormForgetPasswordStateCopyWith(_$_FormForgetPasswordState value,
          $Res Function(_$_FormForgetPasswordState) then) =
      __$$_FormForgetPasswordStateCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {int selectedIndex,
      bool isLoading,
      bool isLoadingCreateNewPassword,
      bool isDisabled,
      bool isOTPSended,
      bool isOtpValidated,
      bool isOTPExpired,
      bool isCompleted,
      Map<String, String> formResetPassword});
}

/// @nodoc
class __$$_FormForgetPasswordStateCopyWithImpl<$Res>
    extends _$FormForgetPasswordStateCopyWithImpl<$Res,
        _$_FormForgetPasswordState>
    implements _$$_FormForgetPasswordStateCopyWith<$Res> {
  __$$_FormForgetPasswordStateCopyWithImpl(_$_FormForgetPasswordState _value,
      $Res Function(_$_FormForgetPasswordState) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? selectedIndex = null,
    Object? isLoading = null,
    Object? isLoadingCreateNewPassword = null,
    Object? isDisabled = null,
    Object? isOTPSended = null,
    Object? isOtpValidated = null,
    Object? isOTPExpired = null,
    Object? isCompleted = null,
    Object? formResetPassword = null,
  }) {
    return _then(_$_FormForgetPasswordState(
      selectedIndex: null == selectedIndex
          ? _value.selectedIndex
          : selectedIndex // ignore: cast_nullable_to_non_nullable
              as int,
      isLoading: null == isLoading
          ? _value.isLoading
          : isLoading // ignore: cast_nullable_to_non_nullable
              as bool,
      isLoadingCreateNewPassword: null == isLoadingCreateNewPassword
          ? _value.isLoadingCreateNewPassword
          : isLoadingCreateNewPassword // ignore: cast_nullable_to_non_nullable
              as bool,
      isDisabled: null == isDisabled
          ? _value.isDisabled
          : isDisabled // ignore: cast_nullable_to_non_nullable
              as bool,
      isOTPSended: null == isOTPSended
          ? _value.isOTPSended
          : isOTPSended // ignore: cast_nullable_to_non_nullable
              as bool,
      isOtpValidated: null == isOtpValidated
          ? _value.isOtpValidated
          : isOtpValidated // ignore: cast_nullable_to_non_nullable
              as bool,
      isOTPExpired: null == isOTPExpired
          ? _value.isOTPExpired
          : isOTPExpired // ignore: cast_nullable_to_non_nullable
              as bool,
      isCompleted: null == isCompleted
          ? _value.isCompleted
          : isCompleted // ignore: cast_nullable_to_non_nullable
              as bool,
      formResetPassword: null == formResetPassword
          ? _value.formResetPassword
          : formResetPassword // ignore: cast_nullable_to_non_nullable
              as Map<String, String>,
    ));
  }
}

/// @nodoc

class _$_FormForgetPasswordState implements _FormForgetPasswordState {
  _$_FormForgetPasswordState(
      {this.selectedIndex = 0,
      this.isLoading = false,
      this.isLoadingCreateNewPassword = false,
      this.isDisabled = true,
      this.isOTPSended = false,
      this.isOtpValidated = false,
      this.isOTPExpired = false,
      this.isCompleted = false,
      this.formResetPassword = const {}});

  @override
  @JsonKey()
  int selectedIndex;
  @override
  @JsonKey()
  bool isLoading;
  @override
  @JsonKey()
  bool isLoadingCreateNewPassword;
  @override
  @JsonKey()
  bool isDisabled;
  @override
  @JsonKey()
  bool isOTPSended;
  @override
  @JsonKey()
  bool isOtpValidated;
  @override
  @JsonKey()
  bool isOTPExpired;
  @override
  @JsonKey()
  bool isCompleted;
  @override
  @JsonKey()
  Map<String, String> formResetPassword;

  @override
  String toString() {
    return 'FormForgetPasswordState(selectedIndex: $selectedIndex, isLoading: $isLoading, isLoadingCreateNewPassword: $isLoadingCreateNewPassword, isDisabled: $isDisabled, isOTPSended: $isOTPSended, isOtpValidated: $isOtpValidated, isOTPExpired: $isOTPExpired, isCompleted: $isCompleted, formResetPassword: $formResetPassword)';
  }

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_FormForgetPasswordStateCopyWith<_$_FormForgetPasswordState>
      get copyWith =>
          __$$_FormForgetPasswordStateCopyWithImpl<_$_FormForgetPasswordState>(
              this, _$identity);
}

abstract class _FormForgetPasswordState implements FormForgetPasswordState {
  factory _FormForgetPasswordState(
      {int selectedIndex,
      bool isLoading,
      bool isLoadingCreateNewPassword,
      bool isDisabled,
      bool isOTPSended,
      bool isOtpValidated,
      bool isOTPExpired,
      bool isCompleted,
      Map<String, String> formResetPassword}) = _$_FormForgetPasswordState;

  @override
  int get selectedIndex;
  set selectedIndex(int value);
  @override
  bool get isLoading;
  set isLoading(bool value);
  @override
  bool get isLoadingCreateNewPassword;
  set isLoadingCreateNewPassword(bool value);
  @override
  bool get isDisabled;
  set isDisabled(bool value);
  @override
  bool get isOTPSended;
  set isOTPSended(bool value);
  @override
  bool get isOtpValidated;
  set isOtpValidated(bool value);
  @override
  bool get isOTPExpired;
  set isOTPExpired(bool value);
  @override
  bool get isCompleted;
  set isCompleted(bool value);
  @override
  Map<String, String> get formResetPassword;
  set formResetPassword(Map<String, String> value);
  @override
  @JsonKey(ignore: true)
  _$$_FormForgetPasswordStateCopyWith<_$_FormForgetPasswordState>
      get copyWith => throw _privateConstructorUsedError;
}
