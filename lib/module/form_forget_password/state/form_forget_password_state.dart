import 'package:freezed_annotation/freezed_annotation.dart';
part 'form_forget_password_state.freezed.dart';

@unfreezed
class FormForgetPasswordState with _$FormForgetPasswordState {
  factory FormForgetPasswordState({
    @Default(0) int selectedIndex,
    @Default(false) bool isLoading,
    @Default(false) bool isLoadingCreateNewPassword,
    @Default(true) bool isDisabled,
    @Default(false) bool isOTPSended,
    @Default(false) bool isOtpValidated,
    @Default(false) bool isOTPExpired,
    @Default(false) bool isCompleted,
    @Default({}) Map<String, String> formResetPassword,
  }) = _FormForgetPasswordState;
}
