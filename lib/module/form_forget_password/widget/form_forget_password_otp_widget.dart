import 'package:bloc_pokedex_app/module/form_forget_password/bloc/form_forget_password_bloc.dart';
import 'package:bloc_pokedex_app/shared/theme/theme_config.dart';
import 'package:bloc_pokedex_app/shared/widget/textfield/otp_textfield.dart';
import 'package:bloc_pokedex_app/shared/widget/typography/pk_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class FFPOtpWidget extends StatefulWidget {
  final String defaultValue;
  final List<FocusNode> focusNode;
  final bool isEnabled;
  final Function(String) onChange;

  const FFPOtpWidget({
    super.key,
    required this.defaultValue,
    required this.focusNode,
    required this.onChange,
    required this.isEnabled,
  });

  @override
  State<FFPOtpWidget> createState() => _FFPOtpWidgetState();
}

class _FFPOtpWidgetState extends State<FFPOtpWidget> {
  late List<String> _otp;

  @override
  void initState() {
    // TODO: implement initState
    widget.focusNode[0].requestFocus();
    _otp = List.filled(widget.focusNode.length, '');
    super.initState();
  }

  void _onOtpChange(int index, String value) {
    if (index < widget.focusNode.length - 1 && value.isNotEmpty) {
      widget.focusNode[index + 1].requestFocus();
    }
    _otp[index] = value;
    widget.onChange(_otp.join());
  }

  @override
  Widget build(BuildContext context) {
    final email = BlocProvider.of<FormForgetPasswordBloc>(context)
        .state
        .formResetPassword['email'];

    return SizedBox(
      width: MediaQuery.of(context).size.width,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          const Spacer(),
          const Center(
            child: PKText(
              label: "Enter the code",
              fontSize: 26,
              fontWeight: FontWeight.w400,
              color: grayDarkerColor,
              textAlign: TextAlign.center,
            ),
          ),
          Center(
            child: PKText(
              label: "Enter the 6-digit code sent to $email",
              fontSize: 26,
              fontWeight: FontWeight.w600,
              textAlign: TextAlign.center,
            ),
          ),
          SizedBox(
            height: 24.0.h,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: List.generate(
              widget.focusNode.length,
              (index) => SizedBox(
                width: 48.w,
                child: PKOtpTextField(
                  isEnabled: widget.isEnabled,
                  onChanged: (value) {
                    _onOtpChange(index, value);
                    //widget.onChange(value);
                  },
                  focusNode: widget.focusNode[index],
                ),
              ),
            ),
          ),
          SizedBox(
            height: 16.0.h,
          ),
          const Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                PKText(
                  label: "Didn't receive the code?",
                  fontSize: 12,
                  fontWeight: FontWeight.w400,
                  color: grayDarkerColor,
                ),
                PKText(
                  label: "Resend in 0:59",
                  fontSize: 12,
                  fontWeight: FontWeight.w600,
                  color: grayDarkerColor,
                ),
              ],
            ),
          ),
          const Spacer(
            flex: 20,
          ),
        ],
      ),
    );
  }
}
