import 'package:bloc_pokedex_app/shared/theme/theme_config.dart';
import 'package:bloc_pokedex_app/shared/widget/textfield/email_textfield.dart';
import 'package:bloc_pokedex_app/shared/widget/typography/pk_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class FFPEmailWidget extends StatelessWidget {
  final String defaultValue;
  final FocusNode focusNode;
  final bool isEnabled;
  final Function(String) onChange;

  const FFPEmailWidget({
    super.key,
    required this.defaultValue,
    required this.focusNode,
    required this.onChange,
    required this.isEnabled,
  });

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: MediaQuery.of(context).size.width,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          const Spacer(),
          const Center(
            child: PKText(
              label: "Let's recover!",
              fontSize: 26,
              fontWeight: FontWeight.w400,
              color: grayDarkerColor,
              textAlign: TextAlign.center,
            ),
          ),
          const Center(
            child: PKText(
              label: "What is your email?",
              fontSize: 26,
              fontWeight: FontWeight.w600,
              textAlign: TextAlign.center,
            ),
          ),
          SizedBox(
            height: 24.0.h,
          ),
          PKEmailTextField(
            isEnabled: isEnabled,
            focusNode: focusNode,
            value: defaultValue,
            hintText: 'E-mail',
            onChanged: (value) {
              onChange(value);
            },
          ),
          SizedBox(
            height: 8.0.h,
          ),
          const Center(
            child: Wrap(
              spacing: 1,
              children: [
                PKText(
                  label: "We will send a verification code to your email.",
                  fontSize: 12,
                  fontWeight: FontWeight.w400,
                  color: grayDarkerColor,
                )
              ],
            ),
          ),
          const Spacer(
            flex: 20,
          ),
        ],
      ),
    );
  }
}
