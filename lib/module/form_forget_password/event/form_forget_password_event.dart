abstract class FormForgetPasswordEvent {}

class FormForgetPasswordOnChangedIndexEvent extends FormForgetPasswordEvent {
  final int selectedIndex;

  FormForgetPasswordOnChangedIndexEvent({
    required this.selectedIndex,
  });
}

class FormForgetPasswordOnInitEvent extends FormForgetPasswordEvent {}

class FormForgetPasswordOnChangeInputFieldEvent
    extends FormForgetPasswordEvent {
  final String email;
  final String otp;
  final String password;

  FormForgetPasswordOnChangeInputFieldEvent({
    required this.email,
    required this.otp,
    required this.password,
  });
}

class FormForgetPasswordOnSendOTPEvent extends FormForgetPasswordEvent {}

class FormForgetPasswordOnValidateOTPEvent extends FormForgetPasswordEvent {}

class FormForgetPasswordOnSubmitNewPasswordEvent
    extends FormForgetPasswordEvent {}

class FormForgetPasswordOnChangeDisabledEvent extends FormForgetPasswordEvent {}
