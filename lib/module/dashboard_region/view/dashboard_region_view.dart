import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import '../bloc/dashboard_region_bloc.dart';
import '../event/dashboard_region_event.dart';
import '../state/dashboard_region_state.dart';

class DashboardRegionView extends StatefulWidget {
  const DashboardRegionView({Key? key}) : super(key: key);

  @override
  State<DashboardRegionView> createState() => _DashboardRegionViewState();
}

class _DashboardRegionViewState extends State<DashboardRegionView> {
  DashboardRegionBloc bloc = DashboardRegionBloc();

  @override
  void initState() {
    bloc.initState();
    super.initState();
  }

  @override
  void dispose() {
    bloc.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (BuildContext context) => bloc,
      child: BlocListener<DashboardRegionBloc, DashboardRegionState>(
        listener: (context, state) {},
        child: BlocBuilder<DashboardRegionBloc, DashboardRegionState>(
          builder: (context, state) {
            final bloc = context.read<DashboardRegionBloc>();
            return buildView(context, bloc, state);
          },
        ),
      ),
    );
  }

  buildView(
    BuildContext context,
    DashboardRegionBloc bloc,
    DashboardRegionState state,
  ) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('DashboardRegion'),
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Text(
            'Counter: ${state.counter}',
            style: const TextStyle(fontSize: 24),
          ),
          IconButton(
            onPressed: () => bloc.add(DashboardRegionIncrementEvent()),
            icon: const Icon(
              Icons.add,
              size: 24.0,
            ),
          ),
        ],
      ),
    );
  }
}
