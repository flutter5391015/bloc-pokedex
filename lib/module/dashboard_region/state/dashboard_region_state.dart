import 'package:freezed_annotation/freezed_annotation.dart';
part 'dashboard_region_state.freezed.dart';

@unfreezed
class DashboardRegionState with _$DashboardRegionState {
  factory DashboardRegionState({
    @Default(0) int counter,
  }) = _DashboardRegionState;
}
