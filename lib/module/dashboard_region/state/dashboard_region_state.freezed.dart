// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'dashboard_region_state.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$DashboardRegionState {
  int get counter => throw _privateConstructorUsedError;
  set counter(int value) => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $DashboardRegionStateCopyWith<DashboardRegionState> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $DashboardRegionStateCopyWith<$Res> {
  factory $DashboardRegionStateCopyWith(DashboardRegionState value,
          $Res Function(DashboardRegionState) then) =
      _$DashboardRegionStateCopyWithImpl<$Res, DashboardRegionState>;
  @useResult
  $Res call({int counter});
}

/// @nodoc
class _$DashboardRegionStateCopyWithImpl<$Res,
        $Val extends DashboardRegionState>
    implements $DashboardRegionStateCopyWith<$Res> {
  _$DashboardRegionStateCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? counter = null,
  }) {
    return _then(_value.copyWith(
      counter: null == counter
          ? _value.counter
          : counter // ignore: cast_nullable_to_non_nullable
              as int,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$_DashboardRegionStateCopyWith<$Res>
    implements $DashboardRegionStateCopyWith<$Res> {
  factory _$$_DashboardRegionStateCopyWith(_$_DashboardRegionState value,
          $Res Function(_$_DashboardRegionState) then) =
      __$$_DashboardRegionStateCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({int counter});
}

/// @nodoc
class __$$_DashboardRegionStateCopyWithImpl<$Res>
    extends _$DashboardRegionStateCopyWithImpl<$Res, _$_DashboardRegionState>
    implements _$$_DashboardRegionStateCopyWith<$Res> {
  __$$_DashboardRegionStateCopyWithImpl(_$_DashboardRegionState _value,
      $Res Function(_$_DashboardRegionState) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? counter = null,
  }) {
    return _then(_$_DashboardRegionState(
      counter: null == counter
          ? _value.counter
          : counter // ignore: cast_nullable_to_non_nullable
              as int,
    ));
  }
}

/// @nodoc

class _$_DashboardRegionState implements _DashboardRegionState {
  _$_DashboardRegionState({this.counter = 0});

  @override
  @JsonKey()
  int counter;

  @override
  String toString() {
    return 'DashboardRegionState(counter: $counter)';
  }

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_DashboardRegionStateCopyWith<_$_DashboardRegionState> get copyWith =>
      __$$_DashboardRegionStateCopyWithImpl<_$_DashboardRegionState>(
          this, _$identity);
}

abstract class _DashboardRegionState implements DashboardRegionState {
  factory _DashboardRegionState({int counter}) = _$_DashboardRegionState;

  @override
  int get counter;
  set counter(int value);
  @override
  @JsonKey(ignore: true)
  _$$_DashboardRegionStateCopyWith<_$_DashboardRegionState> get copyWith =>
      throw _privateConstructorUsedError;
}
