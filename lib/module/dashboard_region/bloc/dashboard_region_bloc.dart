import 'package:flutter_bloc/flutter_bloc.dart';
import '../event/dashboard_region_event.dart';
import '../state/dashboard_region_state.dart';

mixin _BlocLifecycle {
  void initState() {}
  void dispose() {}
}

class DashboardRegionBloc
    extends Bloc<DashboardRegionEvent, DashboardRegionState>
    with _BlocLifecycle {
  DashboardRegionBloc() : super(DashboardRegionState()) {
    on<DashboardRegionIncrementEvent>((event, emit) {
      state.counter++;
      emit(state.copyWith());
    });
  }

  @override
  void initState() {
    //initState event
    super.initState();
  }

  @override
  void dispose() {
    //dispose event
    super.dispose();
  }

  @override
  Future<void> close() {
    dispose();
    return super.close();
  }
}
