import 'package:freezed_annotation/freezed_annotation.dart';
part 'detail_state.freezed.dart';

@unfreezed
class DetailState with _$DetailState {
  factory DetailState({
    @Default(0) int counter,
  }) = _DetailState;
}
