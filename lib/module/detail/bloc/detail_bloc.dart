import 'package:flutter_bloc/flutter_bloc.dart';
import '../event/detail_event.dart';
import '../state/detail_state.dart';

mixin _BlocLifecycle {
  void initState() {}
  void dispose() {}
}

class DetailBloc extends Bloc<DetailEvent, DetailState> with _BlocLifecycle {
  DetailBloc() : super(DetailState()) {
    on<DetailIncrementEvent>((event, emit) {
      state.counter++;
      emit(state.copyWith());
    });
  }

  @override
  void initState() {
    //initState event
    super.initState();
  }

  @override
  void dispose() {
    //dispose event
    super.dispose();
  }

  @override
  Future<void> close() {
    dispose();
    return super.close();
  }
}
