import 'package:freezed_annotation/freezed_annotation.dart';
part 'form_login_email_state.freezed.dart';

@unfreezed
class FormLoginEmailState with _$FormLoginEmailState {
  factory FormLoginEmailState({
    @Default({}) Map<String, String> formLogin,
    @Default(true) bool isDisabled,
    @Default(false) bool isSuccess,
    @Default(false) bool isLoading,
  }) = _FormLoginEmailState;
}
