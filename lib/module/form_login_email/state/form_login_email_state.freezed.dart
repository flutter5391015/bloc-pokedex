// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'form_login_email_state.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$FormLoginEmailState {
  Map<String, String> get formLogin => throw _privateConstructorUsedError;
  set formLogin(Map<String, String> value) =>
      throw _privateConstructorUsedError;
  bool get isDisabled => throw _privateConstructorUsedError;
  set isDisabled(bool value) => throw _privateConstructorUsedError;
  bool get isSuccess => throw _privateConstructorUsedError;
  set isSuccess(bool value) => throw _privateConstructorUsedError;
  bool get isLoading => throw _privateConstructorUsedError;
  set isLoading(bool value) => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $FormLoginEmailStateCopyWith<FormLoginEmailState> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $FormLoginEmailStateCopyWith<$Res> {
  factory $FormLoginEmailStateCopyWith(
          FormLoginEmailState value, $Res Function(FormLoginEmailState) then) =
      _$FormLoginEmailStateCopyWithImpl<$Res, FormLoginEmailState>;
  @useResult
  $Res call(
      {Map<String, String> formLogin,
      bool isDisabled,
      bool isSuccess,
      bool isLoading});
}

/// @nodoc
class _$FormLoginEmailStateCopyWithImpl<$Res, $Val extends FormLoginEmailState>
    implements $FormLoginEmailStateCopyWith<$Res> {
  _$FormLoginEmailStateCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? formLogin = null,
    Object? isDisabled = null,
    Object? isSuccess = null,
    Object? isLoading = null,
  }) {
    return _then(_value.copyWith(
      formLogin: null == formLogin
          ? _value.formLogin
          : formLogin // ignore: cast_nullable_to_non_nullable
              as Map<String, String>,
      isDisabled: null == isDisabled
          ? _value.isDisabled
          : isDisabled // ignore: cast_nullable_to_non_nullable
              as bool,
      isSuccess: null == isSuccess
          ? _value.isSuccess
          : isSuccess // ignore: cast_nullable_to_non_nullable
              as bool,
      isLoading: null == isLoading
          ? _value.isLoading
          : isLoading // ignore: cast_nullable_to_non_nullable
              as bool,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$_FormLoginEmailStateCopyWith<$Res>
    implements $FormLoginEmailStateCopyWith<$Res> {
  factory _$$_FormLoginEmailStateCopyWith(_$_FormLoginEmailState value,
          $Res Function(_$_FormLoginEmailState) then) =
      __$$_FormLoginEmailStateCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {Map<String, String> formLogin,
      bool isDisabled,
      bool isSuccess,
      bool isLoading});
}

/// @nodoc
class __$$_FormLoginEmailStateCopyWithImpl<$Res>
    extends _$FormLoginEmailStateCopyWithImpl<$Res, _$_FormLoginEmailState>
    implements _$$_FormLoginEmailStateCopyWith<$Res> {
  __$$_FormLoginEmailStateCopyWithImpl(_$_FormLoginEmailState _value,
      $Res Function(_$_FormLoginEmailState) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? formLogin = null,
    Object? isDisabled = null,
    Object? isSuccess = null,
    Object? isLoading = null,
  }) {
    return _then(_$_FormLoginEmailState(
      formLogin: null == formLogin
          ? _value.formLogin
          : formLogin // ignore: cast_nullable_to_non_nullable
              as Map<String, String>,
      isDisabled: null == isDisabled
          ? _value.isDisabled
          : isDisabled // ignore: cast_nullable_to_non_nullable
              as bool,
      isSuccess: null == isSuccess
          ? _value.isSuccess
          : isSuccess // ignore: cast_nullable_to_non_nullable
              as bool,
      isLoading: null == isLoading
          ? _value.isLoading
          : isLoading // ignore: cast_nullable_to_non_nullable
              as bool,
    ));
  }
}

/// @nodoc

class _$_FormLoginEmailState implements _FormLoginEmailState {
  _$_FormLoginEmailState(
      {this.formLogin = const {},
      this.isDisabled = true,
      this.isSuccess = false,
      this.isLoading = false});

  @override
  @JsonKey()
  Map<String, String> formLogin;
  @override
  @JsonKey()
  bool isDisabled;
  @override
  @JsonKey()
  bool isSuccess;
  @override
  @JsonKey()
  bool isLoading;

  @override
  String toString() {
    return 'FormLoginEmailState(formLogin: $formLogin, isDisabled: $isDisabled, isSuccess: $isSuccess, isLoading: $isLoading)';
  }

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_FormLoginEmailStateCopyWith<_$_FormLoginEmailState> get copyWith =>
      __$$_FormLoginEmailStateCopyWithImpl<_$_FormLoginEmailState>(
          this, _$identity);
}

abstract class _FormLoginEmailState implements FormLoginEmailState {
  factory _FormLoginEmailState(
      {Map<String, String> formLogin,
      bool isDisabled,
      bool isSuccess,
      bool isLoading}) = _$_FormLoginEmailState;

  @override
  Map<String, String> get formLogin;
  set formLogin(Map<String, String> value);
  @override
  bool get isDisabled;
  set isDisabled(bool value);
  @override
  bool get isSuccess;
  set isSuccess(bool value);
  @override
  bool get isLoading;
  set isLoading(bool value);
  @override
  @JsonKey(ignore: true)
  _$$_FormLoginEmailStateCopyWith<_$_FormLoginEmailState> get copyWith =>
      throw _privateConstructorUsedError;
}
