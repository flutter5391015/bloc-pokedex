import 'package:bloc_pokedex_app/utils/validator.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import '../event/form_login_email_event.dart';
import '../state/form_login_email_state.dart';

mixin _BlocLifecycle {
  void initState() {}
  void dispose() {}
}

class FormLoginEmailBloc extends Bloc<FormLoginEmailEvent, FormLoginEmailState>
    with _BlocLifecycle {
  FormLoginEmailBloc() : super(FormLoginEmailState()) {
    on<FormLoginEmailOnChangeDisabledEvent>((event, emit) {
      state.isDisabled =
          state.formLogin['email'] == '' || state.formLogin['password'] == '';

      emit(state.copyWith());
    });

    on<FormLoginEmailOnChangeInputEvent>((event, emit) {
      var hasRightFormatEmail = Validator.email(event.email);

      state.formLogin = {
        "email": hasRightFormatEmail == null ? event.email! : '',
        "password": event.password ?? '',
      };
      emit(state.copyWith());
      add(FormLoginEmailOnChangeDisabledEvent());
    });

    on<FormLoginEmailOnInitEvent>((event, emit) {
      state.formLogin = {
        "email": '',
        "password": '',
      };

      state.isSuccess = false;
      state.isDisabled = true;
      state.isLoading = false;
      emit(state.copyWith());
    });

    on<FormLoginEmailOnSubmitEvent>((event, emit) async {
      state.isLoading = true;
      emit(state.copyWith());

      await Future.delayed(
        const Duration(seconds: 5),
      );

      state.isLoading = false;
      state.isSuccess = true;
      emit(state.copyWith());
    });

    initState();
  }

  @override
  void initState() {
    //initState event

    add(FormLoginEmailOnInitEvent());
    super.initState();
  }

  @override
  void dispose() {
    //dispose event
    super.dispose();
  }

  @override
  Future<void> close() {
    dispose();
    return super.close();
  }
}
