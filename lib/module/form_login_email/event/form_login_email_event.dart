abstract class FormLoginEmailEvent {}

class FormLoginEmailOnChangeInputEvent extends FormLoginEmailEvent {
  final String? email;
  final String? password;

  FormLoginEmailOnChangeInputEvent({
    this.email,
    this.password,
  });
}

class FormLoginEmailOnChangeDisabledEvent extends FormLoginEmailEvent {}

class FormLoginEmailOnSubmitEvent extends FormLoginEmailEvent {}

class FormLoginEmailOnInitEvent extends FormLoginEmailEvent {}
