import 'package:bloc_pokedex_app/core.dart';
import 'package:bloc_pokedex_app/shared/widget/textfield/email_textfield.dart';
import 'package:bloc_pokedex_app/shared/widget/textfield/password_textfield.dart';
import 'package:bloc_pokedex_app/utils/focus_helper.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class FormLoginEmailView extends StatefulWidget {
  const FormLoginEmailView({Key? key}) : super(key: key);

  @override
  State<FormLoginEmailView> createState() => _FormLoginEmailViewState();
}

class _FormLoginEmailViewState extends State<FormLoginEmailView> {
  FormLoginEmailBloc bloc = FormLoginEmailBloc();
  late FocusNode _emailFocus;
  late FocusNode _passwordFocus;
  late PageController _controller;

  @override
  void initState() {
    bloc.initState();
    _emailFocus = FocusNode();
    _passwordFocus = FocusNode();
    _controller = PageController(initialPage: 0);
    super.initState();
  }

  @override
  void dispose() {
    bloc.dispose();
    _emailFocus.dispose();
    _passwordFocus.dispose();
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (BuildContext context) => bloc,
      child: BlocListener<FormLoginEmailBloc, FormLoginEmailState>(
        listener: (context, state) {
          if (state.isSuccess) {
            Navigator.pushAndRemoveUntil(
              context,
              MaterialPageRoute(
                builder: (context) => const LoginSuccessView(),
              ),
              (Route<dynamic> route) => false,
            );
          }
        },
        child: BlocBuilder<FormLoginEmailBloc, FormLoginEmailState>(
          builder: (context, state) {
            final bloc = context.read<FormLoginEmailBloc>();
            if (state.isLoading) return const LoadingScreenView();

            return buildView(context, bloc, state);
          },
        ),
      ),
    );
  }

  buildView(
    BuildContext context,
    FormLoginEmailBloc bloc,
    FormLoginEmailState state,
  ) {
    final heightKeyboard = MediaQuery.of(context).viewInsets.bottom;
    return GestureDetector(
      onTap: () => FocusHelper.unfocus(context),
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        appBar: AppBar(
          title: const AppBarText(label: 'Login'),
          leading: IconButton(
            icon: const Icon(Icons.arrow_back_ios),
            onPressed: () async {
              FocusHelper.unfocus(context);
              await Future.delayed(
                const Duration(milliseconds: 150),
              );

              if (context.mounted) Navigator.of(context).pop();
            },
          ),
        ),
        body: SingleChildScrollView(
          controller: ScrollController(),
          child: Container(
            width: MediaQuery.of(context).size.width,
            padding: const EdgeInsets.only(
              left: 16,
              right: 16,
              //bottom: 80,
            ).w,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const Center(
                  child: Column(
                    children: [
                      PKText(
                        label: 'Welcome back!',
                        fontWeight: FontWeight.normal,
                        color: grayDarkerColor,
                      ),
                      PKText(
                        label: 'fill in the data',
                        fontWeight: FontWeight.w600,
                        color: blackTextColor,
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  height: 24.0.h,
                ),
                PKEmailTextField(
                  label: 'E-mail',
                  value: state.formLogin['email'],
                  onChanged: (value) => bloc.add(
                    FormLoginEmailOnChangeInputEvent(
                      email: value,
                      password: state.formLogin['password'] ?? '',
                    ),
                  ),
                  focusNode: _emailFocus,
                ),
                SizedBox(
                  height: 16.0.h,
                ),
                PKPasswordTextField(
                  value: state.formLogin['password'],
                  label: 'Password',
                  onChanged: (value) => bloc.add(
                    FormLoginEmailOnChangeInputEvent(
                      password: value,
                      email: state.formLogin['email'] ?? '',
                    ),
                  ),
                  focusNode: _passwordFocus,
                ),
                SizedBox(
                  height: 24.0.h,
                ),
                GestureDetector(
                  onTap: () => Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => const FormForgetPasswordView(),
                    ),
                  ),
                  child: const Center(
                    child: PKText(
                      label: 'Forgot your password?',
                      textAlign: TextAlign.center,
                      color: primaryColor,
                      fontSize: 14,
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                ),
                const SizedBox(
                  height: 24,
                ),
              ],
            ),
          ),
        ),
        bottomNavigationBar: Container(
          margin: EdgeInsets.only(
            left: 16,
            right: 16,
            bottom: heightKeyboard == 0 ? 40 : heightKeyboard + 40,
          ).w,
          child: ButtonPrimary(
            isDisabled: state.isDisabled,
            label: 'Login',
            onPressed: () => state.isDisabled
                ? null
                : bloc.add(
                    FormLoginEmailOnSubmitEvent(),
                  ),
          ),
        ),
      ),
    );
  }
}
