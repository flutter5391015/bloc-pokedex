import 'package:freezed_annotation/freezed_annotation.dart';
part 'form_pin_state.freezed.dart';

@unfreezed
class FormPinState with _$FormPinState {
  factory FormPinState({
    @Default(0) int counter,
  }) = _FormPinState;
}
