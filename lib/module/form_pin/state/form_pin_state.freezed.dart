// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'form_pin_state.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$FormPinState {
  int get counter => throw _privateConstructorUsedError;
  set counter(int value) => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $FormPinStateCopyWith<FormPinState> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $FormPinStateCopyWith<$Res> {
  factory $FormPinStateCopyWith(
          FormPinState value, $Res Function(FormPinState) then) =
      _$FormPinStateCopyWithImpl<$Res, FormPinState>;
  @useResult
  $Res call({int counter});
}

/// @nodoc
class _$FormPinStateCopyWithImpl<$Res, $Val extends FormPinState>
    implements $FormPinStateCopyWith<$Res> {
  _$FormPinStateCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? counter = null,
  }) {
    return _then(_value.copyWith(
      counter: null == counter
          ? _value.counter
          : counter // ignore: cast_nullable_to_non_nullable
              as int,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$_FormPinStateCopyWith<$Res>
    implements $FormPinStateCopyWith<$Res> {
  factory _$$_FormPinStateCopyWith(
          _$_FormPinState value, $Res Function(_$_FormPinState) then) =
      __$$_FormPinStateCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({int counter});
}

/// @nodoc
class __$$_FormPinStateCopyWithImpl<$Res>
    extends _$FormPinStateCopyWithImpl<$Res, _$_FormPinState>
    implements _$$_FormPinStateCopyWith<$Res> {
  __$$_FormPinStateCopyWithImpl(
      _$_FormPinState _value, $Res Function(_$_FormPinState) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? counter = null,
  }) {
    return _then(_$_FormPinState(
      counter: null == counter
          ? _value.counter
          : counter // ignore: cast_nullable_to_non_nullable
              as int,
    ));
  }
}

/// @nodoc

class _$_FormPinState implements _FormPinState {
  _$_FormPinState({this.counter = 0});

  @override
  @JsonKey()
  int counter;

  @override
  String toString() {
    return 'FormPinState(counter: $counter)';
  }

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_FormPinStateCopyWith<_$_FormPinState> get copyWith =>
      __$$_FormPinStateCopyWithImpl<_$_FormPinState>(this, _$identity);
}

abstract class _FormPinState implements FormPinState {
  factory _FormPinState({int counter}) = _$_FormPinState;

  @override
  int get counter;
  set counter(int value);
  @override
  @JsonKey(ignore: true)
  _$$_FormPinStateCopyWith<_$_FormPinState> get copyWith =>
      throw _privateConstructorUsedError;
}
