import 'package:flutter_bloc/flutter_bloc.dart';
import '../event/form_pin_event.dart';
import '../state/form_pin_state.dart';

mixin _BlocLifecycle {
  void initState() {}
  void dispose() {}
}

class FormPinBloc extends Bloc<FormPinEvent, FormPinState> with _BlocLifecycle {
  FormPinBloc() : super(FormPinState()) {
    on<FormPinIncrementEvent>((event, emit) {
      state.counter++;
      emit(state.copyWith());
    });
  }

  @override
  void initState() {
    //initState event
    super.initState();
  }

  @override
  void dispose() {
    //dispose event
    super.dispose();
  }

  @override
  Future<void> close() {
    dispose();
    return super.close();
  }
}
