import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import '../bloc/form_pin_bloc.dart';
import '../event/form_pin_event.dart';
import '../state/form_pin_state.dart';

class FormPinView extends StatefulWidget {
  const FormPinView({Key? key}) : super(key: key);

  @override
  State<FormPinView> createState() => _FormPinViewState();
}

class _FormPinViewState extends State<FormPinView> {
  FormPinBloc bloc = FormPinBloc();

  @override
  void initState() {
    bloc.initState();
    super.initState();
  }

  @override
  void dispose() {
    bloc.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (BuildContext context) => bloc,
      child: BlocListener<FormPinBloc, FormPinState>(
        listener: (context, state) {},
        child: BlocBuilder<FormPinBloc, FormPinState>(
          builder: (context, state) {
            final bloc = context.read<FormPinBloc>();
            return buildView(context, bloc, state);
          },
        ),
      ),
    );
  }

  buildView(
    BuildContext context,
    FormPinBloc bloc,
    FormPinState state,
  ) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('FormPin'),
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Text(
            'Counter: ${state.counter}',
            style: const TextStyle(fontSize: 24),
          ),
          IconButton(
            onPressed: () => bloc.add(FormPinIncrementEvent()),
            icon: const Icon(
              Icons.add,
              size: 24.0,
            ),
          ),
        ],
      ),
    );
  }
}
