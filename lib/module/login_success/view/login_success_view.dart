import 'package:bloc_pokedex_app/module/home/view/home_view.dart';
import 'package:bloc_pokedex_app/shared/theme/theme_config.dart';
import 'package:bloc_pokedex_app/shared/widget/button/button_primary.dart';
import 'package:bloc_pokedex_app/shared/widget/typography/pk_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class LoginSuccessView extends StatelessWidget {
  const LoginSuccessView({super.key});

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.white,
        body: Column(
          children: [
            Expanded(
              child: Image.asset(
                "assets/images/login_success.png",
              ),
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              padding: const EdgeInsets.only(
                bottom: 40,
                left: 16,
                right: 16,
              ).h,
              child: Column(
                children: [
                  const PKText(
                    label: 'Welcome back, Trainer!',
                    fontWeight: FontWeight.w600,
                    textAlign: TextAlign.center,
                  ),
                  SizedBox(
                    height: 16.0.h,
                  ),
                  const PKText(
                    label:
                        "We hope you've had a long journey since your last visit.",
                    fontWeight: FontWeight.normal,
                    textAlign: TextAlign.center,
                    fontSize: 14,
                    color: grayDarkerColor,
                  ),
                  SizedBox(
                    height: 32.0.h,
                  ),
                  ButtonPrimary(
                    label: 'Continue',
                    onPressed: () => Navigator.pushReplacement(
                      context,
                      MaterialPageRoute(
                        builder: (context) => const HomeView(),
                      ),
                    ),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
