import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import '../event/form_register_email_event.dart';
import '../state/form_register_email_state.dart';

mixin _BlocLifecycle {
  void initState() {}
  void dispose() {}
}

class FormRegisterEmailBloc
    extends Bloc<FormRegisterEmailEvent, FormRegisterEmailState>
    with _BlocLifecycle {
  FormRegisterEmailBloc() : super(FormRegisterEmailState()) {
    on<FormRegisterEmailOnChangeIndexEvent>((event, emit) {
      state.emailFocusNode!.unfocus();
      state.nameFocusNode!.unfocus();
      state.passwordFocusNode!.unfocus();

      if (event.index == 0) {
        state.emailFocusNode!.requestFocus();
      } else if (event.index == 1) {
        state.passwordFocusNode!.requestFocus();
      } else if (event.index == 2) {
        state.nameFocusNode!.requestFocus();
      }
      state.curIndex = event.index;
      emit(state.copyWith());
      add(FormRegisterEmailOnChangeDisabledEvent());
    });

    on<FormRegisterEmailOnChangeDisabledEvent>((event, emit) {
      if (state.curIndex == 0) {
        String email = state.formRegister['email'] as String;
        state.isDisabled = email != '' ? false : true;
      } else if (state.curIndex == 1) {
        String password = state.formRegister['password'] as String;
        state.isDisabled = password != '' ? false : true;
      } else {
        String name = state.formRegister['name'] as String;
        state.isDisabled = name != '' ? false : true;
      }

      emit(state.copyWith());
    });

    on<FormRegisterEmailOnChangeInputEvent>((event, emit) {
      state.formRegister = {
        "email": event.email ?? '',
        "password": event.password ?? '',
        "name": event.name ?? '',
      };
      emit(state.copyWith());
      add(FormRegisterEmailOnChangeDisabledEvent());
    });

    on<FormRegisterEmailOnInitEvent>((event, emit) {
      state.emailFocusNode = FocusNode();
      state.nameFocusNode = FocusNode();
      state.passwordFocusNode = FocusNode();
      state.formRegister = {
        "email": "",
        "password": "",
        "name": "",
      };
      emit(state.copyWith());
      add(FormRegisterEmailOnChangeDisabledEvent());
    });

    on<FormRegisterEmailOnSubmitEvent>((event, emit) async {
      state.isLoading = true;
      emit(state.copyWith());

      await Future.delayed(
        const Duration(seconds: 3),
      );

      state.isLoading = false;
      state.isCompleted = true;
      emit(state.copyWith());
    });

    initState();
  }

  @override
  void initState() async {
    //initState event
    add(
      FormRegisterEmailOnInitEvent(),
    );

    await Future.delayed(
      const Duration(milliseconds: 500),
    );

    add(
      FormRegisterEmailOnChangeIndexEvent(index: 0),
    );
    super.initState();
  }

  @override
  void dispose() {
    //dispose event
    state.emailFocusNode!.dispose();
    state.nameFocusNode!.dispose();
    state.passwordFocusNode!.dispose();
    super.dispose();
  }

  @override
  Future<void> close() {
    dispose();
    return super.close();
  }
}
