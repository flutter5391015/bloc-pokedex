import 'package:bloc_pokedex_app/shared/theme/theme_config.dart';
import 'package:bloc_pokedex_app/shared/widget/textfield/password_textfield.dart';
import 'package:bloc_pokedex_app/shared/widget/typography/pk_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class FormRegisterPassword extends StatelessWidget {
  final Function(String) onChange;
  final FocusNode focusNode;
  final String defaultValue;

  const FormRegisterPassword({
    super.key,
    required this.onChange,
    required this.focusNode,
    required this.defaultValue,
  });

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: MediaQuery.of(context).size.width,
      child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            const Spacer(),
            const PKText(
              label: "Now...",
              fontSize: 26,
              fontWeight: FontWeight.w400,
              color: grayDarkerColor,
            ),
            const PKText(
              label: "Create a password",
              fontSize: 26,
              fontWeight: FontWeight.w600,
            ),
            SizedBox(
              height: 24.0.h,
            ),
            PKPasswordTextField(
              focusNode: focusNode,
              hintText: 'Password',
              value: defaultValue,
              onChanged: (value) => onChange(value),
            ),
            SizedBox(
              height: 8.0.h,
            ),
            const PKText(
              label: "Your password must be at least 8 characters long.",
              fontSize: 12,
              fontWeight: FontWeight.w400,
              color: grayDarkerColor,
            ),
            const Spacer(
              flex: 20,
            ),
          ]),
    );
  }
}
