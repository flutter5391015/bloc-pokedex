import 'package:bloc_pokedex_app/shared/theme/theme_config.dart';
import 'package:bloc_pokedex_app/shared/widget/textfield/common_textfield.dart';
import 'package:bloc_pokedex_app/shared/widget/typography/pk_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class FormRegisterNameWidget extends StatelessWidget {
  final String defaultValue;
  final FocusNode focusNode;
  final Function(String) onChange;

  const FormRegisterNameWidget({
    super.key,
    required this.onChange,
    required this.defaultValue,
    required this.focusNode,
  });

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: MediaQuery.of(context).size.width,
      child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            const Spacer(),
            const PKText(
              label: "To finish",
              fontSize: 26,
              fontWeight: FontWeight.w400,
              color: grayDarkerColor,
            ),
            const PKText(
              label: "What is your name?",
              fontSize: 26,
              fontWeight: FontWeight.w600,
            ),
            SizedBox(
              height: 24.0.h,
            ),
            PKCommonTextField(
              focusNode: focusNode,
              value: defaultValue,
              hintText: 'Name',
              onChanged: (value) {
                onChange(value);
              },
            ),
            SizedBox(
              height: 8.0.h,
            ),
            const PKText(
              label: "This will be your in-app username.",
              fontSize: 12,
              fontWeight: FontWeight.w400,
              color: grayDarkerColor,
            ),
            const Spacer(
              flex: 20,
            ),
          ]),
    );
  }
}
