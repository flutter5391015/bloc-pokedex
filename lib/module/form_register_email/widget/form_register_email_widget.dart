import 'package:bloc_pokedex_app/shared/theme/theme_config.dart';
import 'package:bloc_pokedex_app/shared/widget/textfield/email_textfield.dart';
import 'package:bloc_pokedex_app/shared/widget/typography/pk_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class FormRegisterEmailWidget extends StatelessWidget {
  final String defaultValue;
  final FocusNode focusNode;
  final Function(String) onChange;

  const FormRegisterEmailWidget({
    super.key,
    required this.focusNode,
    required this.onChange,
    required this.defaultValue,
  });

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: MediaQuery.of(context).size.width,
      child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            const Spacer(),
            const PKText(
              label: "Let's start!",
              fontSize: 26,
              fontWeight: FontWeight.w400,
              color: grayDarkerColor,
            ),
            const PKText(
              label: "What is your email?",
              fontSize: 26,
              fontWeight: FontWeight.w600,
            ),
            SizedBox(
              height: 24.0.h,
            ),
            PKEmailTextField(
              focusNode: focusNode,
              value: defaultValue,
              hintText: 'E-mail',
              onChanged: (value) {
                onChange(value);
              },
            ),
            SizedBox(
              height: 8.0.h,
            ),
            const PKText(
              label: "Please use a valid email address.",
              fontSize: 12,
              fontWeight: FontWeight.w400,
              color: grayDarkerColor,
            ),
            const Spacer(
              flex: 20,
            ),
          ]),
    );
  }
}
