abstract class FormRegisterEmailEvent {}

class FormRegisterEmailOnChangeInputEvent extends FormRegisterEmailEvent {
  final String? email;
  final String? password;
  final String? name;

  FormRegisterEmailOnChangeInputEvent({
    this.email,
    this.password,
    this.name,
  });
}

class FormRegisterEmailOnChangeIndexEvent extends FormRegisterEmailEvent {
  final int index;

  FormRegisterEmailOnChangeIndexEvent({
    required this.index,
  });
}

class FormRegisterEmailOnChangeDisabledEvent extends FormRegisterEmailEvent {}

class FormRegisterEmailOnInitEvent extends FormRegisterEmailEvent {}

class FormRegisterEmailOnSubmitEvent extends FormRegisterEmailEvent {}
