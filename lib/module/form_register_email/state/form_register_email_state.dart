import 'package:flutter/material.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
part 'form_register_email_state.freezed.dart';

@unfreezed
class FormRegisterEmailState with _$FormRegisterEmailState {
  factory FormRegisterEmailState({
    @Default(0) int curIndex,
    @Default(true) bool isDisabled,
    @Default(false) bool isLoading,
    @Default(false) bool isCompleted,
    @Default({}) Map<String, String> formRegister,
    FocusNode? emailFocusNode,
    FocusNode? passwordFocusNode,
    FocusNode? nameFocusNode,
  }) = _FormRegisterEmailState;
}
