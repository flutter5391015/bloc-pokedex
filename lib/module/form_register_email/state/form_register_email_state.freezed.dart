// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'form_register_email_state.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$FormRegisterEmailState {
  int get curIndex => throw _privateConstructorUsedError;
  set curIndex(int value) => throw _privateConstructorUsedError;
  bool get isDisabled => throw _privateConstructorUsedError;
  set isDisabled(bool value) => throw _privateConstructorUsedError;
  bool get isLoading => throw _privateConstructorUsedError;
  set isLoading(bool value) => throw _privateConstructorUsedError;
  bool get isCompleted => throw _privateConstructorUsedError;
  set isCompleted(bool value) => throw _privateConstructorUsedError;
  Map<String, String> get formRegister => throw _privateConstructorUsedError;
  set formRegister(Map<String, String> value) =>
      throw _privateConstructorUsedError;
  FocusNode? get emailFocusNode => throw _privateConstructorUsedError;
  set emailFocusNode(FocusNode? value) => throw _privateConstructorUsedError;
  FocusNode? get passwordFocusNode => throw _privateConstructorUsedError;
  set passwordFocusNode(FocusNode? value) => throw _privateConstructorUsedError;
  FocusNode? get nameFocusNode => throw _privateConstructorUsedError;
  set nameFocusNode(FocusNode? value) => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $FormRegisterEmailStateCopyWith<FormRegisterEmailState> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $FormRegisterEmailStateCopyWith<$Res> {
  factory $FormRegisterEmailStateCopyWith(FormRegisterEmailState value,
          $Res Function(FormRegisterEmailState) then) =
      _$FormRegisterEmailStateCopyWithImpl<$Res, FormRegisterEmailState>;
  @useResult
  $Res call(
      {int curIndex,
      bool isDisabled,
      bool isLoading,
      bool isCompleted,
      Map<String, String> formRegister,
      FocusNode? emailFocusNode,
      FocusNode? passwordFocusNode,
      FocusNode? nameFocusNode});
}

/// @nodoc
class _$FormRegisterEmailStateCopyWithImpl<$Res,
        $Val extends FormRegisterEmailState>
    implements $FormRegisterEmailStateCopyWith<$Res> {
  _$FormRegisterEmailStateCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? curIndex = null,
    Object? isDisabled = null,
    Object? isLoading = null,
    Object? isCompleted = null,
    Object? formRegister = null,
    Object? emailFocusNode = freezed,
    Object? passwordFocusNode = freezed,
    Object? nameFocusNode = freezed,
  }) {
    return _then(_value.copyWith(
      curIndex: null == curIndex
          ? _value.curIndex
          : curIndex // ignore: cast_nullable_to_non_nullable
              as int,
      isDisabled: null == isDisabled
          ? _value.isDisabled
          : isDisabled // ignore: cast_nullable_to_non_nullable
              as bool,
      isLoading: null == isLoading
          ? _value.isLoading
          : isLoading // ignore: cast_nullable_to_non_nullable
              as bool,
      isCompleted: null == isCompleted
          ? _value.isCompleted
          : isCompleted // ignore: cast_nullable_to_non_nullable
              as bool,
      formRegister: null == formRegister
          ? _value.formRegister
          : formRegister // ignore: cast_nullable_to_non_nullable
              as Map<String, String>,
      emailFocusNode: freezed == emailFocusNode
          ? _value.emailFocusNode
          : emailFocusNode // ignore: cast_nullable_to_non_nullable
              as FocusNode?,
      passwordFocusNode: freezed == passwordFocusNode
          ? _value.passwordFocusNode
          : passwordFocusNode // ignore: cast_nullable_to_non_nullable
              as FocusNode?,
      nameFocusNode: freezed == nameFocusNode
          ? _value.nameFocusNode
          : nameFocusNode // ignore: cast_nullable_to_non_nullable
              as FocusNode?,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$_FormRegisterEmailStateCopyWith<$Res>
    implements $FormRegisterEmailStateCopyWith<$Res> {
  factory _$$_FormRegisterEmailStateCopyWith(_$_FormRegisterEmailState value,
          $Res Function(_$_FormRegisterEmailState) then) =
      __$$_FormRegisterEmailStateCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {int curIndex,
      bool isDisabled,
      bool isLoading,
      bool isCompleted,
      Map<String, String> formRegister,
      FocusNode? emailFocusNode,
      FocusNode? passwordFocusNode,
      FocusNode? nameFocusNode});
}

/// @nodoc
class __$$_FormRegisterEmailStateCopyWithImpl<$Res>
    extends _$FormRegisterEmailStateCopyWithImpl<$Res,
        _$_FormRegisterEmailState>
    implements _$$_FormRegisterEmailStateCopyWith<$Res> {
  __$$_FormRegisterEmailStateCopyWithImpl(_$_FormRegisterEmailState _value,
      $Res Function(_$_FormRegisterEmailState) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? curIndex = null,
    Object? isDisabled = null,
    Object? isLoading = null,
    Object? isCompleted = null,
    Object? formRegister = null,
    Object? emailFocusNode = freezed,
    Object? passwordFocusNode = freezed,
    Object? nameFocusNode = freezed,
  }) {
    return _then(_$_FormRegisterEmailState(
      curIndex: null == curIndex
          ? _value.curIndex
          : curIndex // ignore: cast_nullable_to_non_nullable
              as int,
      isDisabled: null == isDisabled
          ? _value.isDisabled
          : isDisabled // ignore: cast_nullable_to_non_nullable
              as bool,
      isLoading: null == isLoading
          ? _value.isLoading
          : isLoading // ignore: cast_nullable_to_non_nullable
              as bool,
      isCompleted: null == isCompleted
          ? _value.isCompleted
          : isCompleted // ignore: cast_nullable_to_non_nullable
              as bool,
      formRegister: null == formRegister
          ? _value.formRegister
          : formRegister // ignore: cast_nullable_to_non_nullable
              as Map<String, String>,
      emailFocusNode: freezed == emailFocusNode
          ? _value.emailFocusNode
          : emailFocusNode // ignore: cast_nullable_to_non_nullable
              as FocusNode?,
      passwordFocusNode: freezed == passwordFocusNode
          ? _value.passwordFocusNode
          : passwordFocusNode // ignore: cast_nullable_to_non_nullable
              as FocusNode?,
      nameFocusNode: freezed == nameFocusNode
          ? _value.nameFocusNode
          : nameFocusNode // ignore: cast_nullable_to_non_nullable
              as FocusNode?,
    ));
  }
}

/// @nodoc

class _$_FormRegisterEmailState implements _FormRegisterEmailState {
  _$_FormRegisterEmailState(
      {this.curIndex = 0,
      this.isDisabled = true,
      this.isLoading = false,
      this.isCompleted = false,
      this.formRegister = const {},
      this.emailFocusNode,
      this.passwordFocusNode,
      this.nameFocusNode});

  @override
  @JsonKey()
  int curIndex;
  @override
  @JsonKey()
  bool isDisabled;
  @override
  @JsonKey()
  bool isLoading;
  @override
  @JsonKey()
  bool isCompleted;
  @override
  @JsonKey()
  Map<String, String> formRegister;
  @override
  FocusNode? emailFocusNode;
  @override
  FocusNode? passwordFocusNode;
  @override
  FocusNode? nameFocusNode;

  @override
  String toString() {
    return 'FormRegisterEmailState(curIndex: $curIndex, isDisabled: $isDisabled, isLoading: $isLoading, isCompleted: $isCompleted, formRegister: $formRegister, emailFocusNode: $emailFocusNode, passwordFocusNode: $passwordFocusNode, nameFocusNode: $nameFocusNode)';
  }

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_FormRegisterEmailStateCopyWith<_$_FormRegisterEmailState> get copyWith =>
      __$$_FormRegisterEmailStateCopyWithImpl<_$_FormRegisterEmailState>(
          this, _$identity);
}

abstract class _FormRegisterEmailState implements FormRegisterEmailState {
  factory _FormRegisterEmailState(
      {int curIndex,
      bool isDisabled,
      bool isLoading,
      bool isCompleted,
      Map<String, String> formRegister,
      FocusNode? emailFocusNode,
      FocusNode? passwordFocusNode,
      FocusNode? nameFocusNode}) = _$_FormRegisterEmailState;

  @override
  int get curIndex;
  set curIndex(int value);
  @override
  bool get isDisabled;
  set isDisabled(bool value);
  @override
  bool get isLoading;
  set isLoading(bool value);
  @override
  bool get isCompleted;
  set isCompleted(bool value);
  @override
  Map<String, String> get formRegister;
  set formRegister(Map<String, String> value);
  @override
  FocusNode? get emailFocusNode;
  set emailFocusNode(FocusNode? value);
  @override
  FocusNode? get passwordFocusNode;
  set passwordFocusNode(FocusNode? value);
  @override
  FocusNode? get nameFocusNode;
  set nameFocusNode(FocusNode? value);
  @override
  @JsonKey(ignore: true)
  _$$_FormRegisterEmailStateCopyWith<_$_FormRegisterEmailState> get copyWith =>
      throw _privateConstructorUsedError;
}
