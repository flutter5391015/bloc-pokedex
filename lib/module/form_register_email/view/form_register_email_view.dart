import 'package:bloc_pokedex_app/module/form_register_email/widget/form_register_email_widget.dart';
import 'package:bloc_pokedex_app/module/form_register_email/widget/form_register_name_widget.dart';
import 'package:bloc_pokedex_app/module/form_register_email/widget/form_register_password_widget.dart';
import 'package:bloc_pokedex_app/module/register_success/view/register_success_view.dart';
import 'package:bloc_pokedex_app/shared/widget/loading_screen/loading_screen_view.dart';
import 'package:bloc_pokedex_app/shared/widget/button/button_primary.dart';
import 'package:bloc_pokedex_app/shared/widget/typography/appbar_text.dart';
import 'package:bloc_pokedex_app/utils/focus_helper.dart';
import 'package:bloc_pokedex_app/utils/validator.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import '../bloc/form_register_email_bloc.dart';
import '../event/form_register_email_event.dart';
import '../state/form_register_email_state.dart';

class FormRegisterEmailView extends StatefulWidget {
  const FormRegisterEmailView({Key? key}) : super(key: key);

  @override
  State<FormRegisterEmailView> createState() => _FormRegisterEmailViewState();
}

class _FormRegisterEmailViewState extends State<FormRegisterEmailView> {
  FormRegisterEmailBloc bloc = FormRegisterEmailBloc();
  late PageController _controller;
  @override
  void initState() {
    bloc.initState();
    _controller = PageController(initialPage: 0);
    super.initState();
  }

  @override
  void dispose() {
    bloc.dispose();
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (BuildContext context) => bloc,
      child: BlocListener<FormRegisterEmailBloc, FormRegisterEmailState>(
        listener: (context, state) {
          if (state.isCompleted) {
            Navigator.pushAndRemoveUntil(
              context,
              MaterialPageRoute(
                builder: (context) => const RegisterSuccessView(),
              ),
              (Route<dynamic> route) => false,
            );
          }
        },
        child: BlocBuilder<FormRegisterEmailBloc, FormRegisterEmailState>(
          builder: (context, state) {
            final bloc = context.read<FormRegisterEmailBloc>();

            if (state.isLoading) return const LoadingScreenView();
            return buildView(context, bloc, state);
          },
        ),
      ),
    );
  }

  buildView(
    BuildContext context,
    FormRegisterEmailBloc bloc,
    FormRegisterEmailState state,
  ) {
    return GestureDetector(
      onTap: () => FocusHelper.unfocus(context),
      child: Scaffold(
        appBar: AppBar(
          title: const AppBarText(label: 'Create an account'),
          leading: IconButton(
            icon: const Icon(Icons.arrow_back_ios),
            onPressed: () async {
              if (state.curIndex == 0) {
                FocusHelper.unfocus(context);
                await Future.delayed(
                  const Duration(milliseconds: 150),
                );

                if (context.mounted) Navigator.of(context).pop();
              } else {
                FocusHelper.unfocus(context);
                _controller.previousPage(
                    duration: const Duration(milliseconds: 150),
                    curve: Curves.bounceInOut);
              }
            },
          ),
        ),
        body: Container(
          width: MediaQuery.of(context).size.width,
          padding: const EdgeInsets.only(
            left: 16,
            right: 16,
            bottom: 40,
          ).w,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Expanded(
                child: PageView.builder(
                  itemCount: 3,
                  controller: _controller,
                  onPageChanged: (value) {
                    bloc.add(
                      FormRegisterEmailOnChangeIndexEvent(index: value),
                    );
                  },
                  itemBuilder: (context, index) {
                    if (index == 0) {
                      return FormRegisterEmailWidget(
                        focusNode: state.emailFocusNode!,
                        defaultValue: state.formRegister['email'] ?? '',
                        onChange: (value) {
                          var hasRightFormatEmail = Validator.email(value);
                          bloc.add(
                            FormRegisterEmailOnChangeInputEvent(
                              email: hasRightFormatEmail == null ? value : '',
                            ),
                          );
                        },
                      );
                    } else if (index == 1) {
                      return FormRegisterPassword(
                        focusNode: state.passwordFocusNode!,
                        defaultValue: state.formRegister['password'] ?? '',
                        onChange: (value) {
                          bloc.add(
                            FormRegisterEmailOnChangeInputEvent(
                              email: state.formRegister['email'],
                              password:
                                  value.trim() != '' && value.trim().length >= 8
                                      ? value
                                      : '',
                            ),
                          );
                        },
                      );
                    }

                    return FormRegisterNameWidget(
                      focusNode: state.nameFocusNode!,
                      onChange: (value) {
                        bloc.add(
                          FormRegisterEmailOnChangeInputEvent(
                            email: state.formRegister['email'],
                            password: state.formRegister['password'],
                            name: value,
                          ),
                        );
                      },
                      defaultValue: state.formRegister['name'] ?? '',
                    );
                  },
                ),
              ),
              ButtonPrimary(
                isDisabled: state.isDisabled,
                label: state.curIndex == 2 ? 'Create an account' : 'Continue',
                onPressed: () {
                  if (state.curIndex == 2) {
                    bloc.add(FormRegisterEmailOnSubmitEvent());
                  } else {
                    _controller.nextPage(
                      duration: const Duration(milliseconds: 150),
                      curve: Curves.bounceIn,
                    );
                  }
                },
              )
            ],
          ),
        ),
      ),
    );
  }
}
