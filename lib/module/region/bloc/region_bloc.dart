import 'package:flutter_bloc/flutter_bloc.dart';
import '../event/region_event.dart';
import '../state/region_state.dart';

mixin _BlocLifecycle {
  void initState() {}
  void dispose() {}
}

class RegionBloc extends Bloc<RegionEvent, RegionState> with _BlocLifecycle {
  RegionBloc() : super(RegionState()) {
    on<RegionIncrementEvent>((event, emit) {
      state.counter++;
      emit(state.copyWith());
    });
  }

  @override
  void initState() {
    //initState event
    super.initState();
  }

  @override
  void dispose() {
    //dispose event
    super.dispose();
  }

  @override
  Future<void> close() {
    dispose();
    return super.close();
  }
}
