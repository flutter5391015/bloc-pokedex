import 'package:freezed_annotation/freezed_annotation.dart';
part 'region_state.freezed.dart';

@unfreezed
class RegionState with _$RegionState {
  factory RegionState({
    @Default(0) int counter,
  }) = _RegionState;
}
