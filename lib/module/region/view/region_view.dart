import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import '../bloc/region_bloc.dart';
import '../event/region_event.dart';
import '../state/region_state.dart';

class RegionView extends StatefulWidget {
  const RegionView({Key? key}) : super(key: key);

  @override
  State<RegionView> createState() => _RegionViewState();
}

class _RegionViewState extends State<RegionView> {
  RegionBloc bloc = RegionBloc();

  @override
  void initState() {
    bloc.initState();
    super.initState();
  }

  @override
  void dispose() {
    bloc.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (BuildContext context) => bloc,
      child: BlocListener<RegionBloc, RegionState>(
        listener: (context, state) {},
        child: BlocBuilder<RegionBloc, RegionState>(
          builder: (context, state) {
            final bloc = context.read<RegionBloc>();
            return buildView(context, bloc, state);
          },
        ),
      ),
    );
  }

  buildView(
    BuildContext context,
    RegionBloc bloc,
    RegionState state,
  ) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Region'),
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Text(
            'Counter: ${state.counter}',
            style: const TextStyle(fontSize: 24),
          ),
          IconButton(
            onPressed: () => bloc.add(RegionIncrementEvent()),
            icon: const Icon(
              Icons.add,
              size: 24.0,
            ),
          ),
        ],
      ),
    );
  }
}
