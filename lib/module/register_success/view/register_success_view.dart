import 'package:bloc_pokedex_app/module/home/view/home_view.dart';
import 'package:bloc_pokedex_app/shared/theme/theme_config.dart';
import 'package:bloc_pokedex_app/shared/widget/button/button_primary.dart';
import 'package:bloc_pokedex_app/shared/widget/typography/pk_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';

class RegisterSuccessView extends StatelessWidget {
  const RegisterSuccessView({super.key});

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.white,
        body: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Expanded(
              child: Stack(
                children: [
                  Positioned(
                    top: 0,
                    left: 0,
                    child: SvgPicture.asset('assets/images/icon_success.svg'),
                  ),
                  Positioned(
                    top: 150,
                    left: 0,
                    right: 0,
                    child: SizedBox(
                      height: 325.h,
                      child: Image.asset(
                        "assets/images/register_success.png",
                        fit: BoxFit.contain,
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              padding: const EdgeInsets.only(
                bottom: 40,
                left: 16,
                right: 16,
              ).h,
              child: Column(
                children: [
                  const PKText(
                    label: 'Your account was successfully created!',
                    fontWeight: FontWeight.w600,
                    textAlign: TextAlign.center,
                  ),
                  SizedBox(
                    height: 16.0.h,
                  ),
                  const PKText(
                    label:
                        'Welcome, coach! We are excited to follow your journey.',
                    fontWeight: FontWeight.normal,
                    textAlign: TextAlign.center,
                    fontSize: 14,
                    color: grayDarkerColor,
                  ),
                  SizedBox(
                    height: 32.0.h,
                  ),
                  ButtonPrimary(
                    label: 'Continue',
                    onPressed: () => Navigator.pushReplacement(
                      context,
                      MaterialPageRoute(
                        builder: (context) => const HomeView(),
                      ),
                    ),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
