import 'package:freezed_annotation/freezed_annotation.dart';
part 'onboarding_state.freezed.dart';

@unfreezed
class OnboardingState with _$OnboardingState {
  factory OnboardingState({
    @Default(0) int selectedIndex,
    @Default([]) List<Map<String, String>> onBoardingMessage,
  }) = _OnboardingState;
}
