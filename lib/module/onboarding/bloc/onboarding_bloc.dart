import 'package:flutter_bloc/flutter_bloc.dart';
import '../event/onboarding_event.dart';
import '../state/onboarding_state.dart';

mixin _BlocLifecycle {
  void initState() {}
  void dispose() {}
}

class OnboardingBloc extends Bloc<OnboardingEvent, OnboardingState>
    with _BlocLifecycle {
  OnboardingBloc() : super(OnboardingState()) {
    on<OnboardingOnLoadEvent>((event, emit) {
      state.onBoardingMessage = [
        {
          "image": "assets/images/onboarding.png",
          "title": "All Pokémon in One Place",
          "message":
              "Access a vast list of Pokémon from every generation ever made by Nintendo",
        },
        {
          "image": "assets/images/onboarding2.png",
          "title": "keep your updated pokedex",
          "message":
              "Register and keep your profile, favorite Pokemon, settings and much more saved in the app",
        },
      ];
      emit(state.copyWith());
    });
    on<OnboardingOnPageChangeEvent>((event, emit) {
      state.selectedIndex = event.index;
      emit(state.copyWith());
    });
    initState();
  }

  @override
  void initState() {
    //initState event
    super.initState();
    add(OnboardingOnLoadEvent());
  }

  @override
  void dispose() {
    //dispose event
    super.dispose();
  }

  @override
  Future<void> close() {
    dispose();
    return super.close();
  }
}
