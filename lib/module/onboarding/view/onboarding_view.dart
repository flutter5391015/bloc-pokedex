import 'package:bloc_pokedex_app/core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:shared_preferences/shared_preferences.dart';

class OnboardingView extends StatefulWidget {
  const OnboardingView({Key? key}) : super(key: key);

  @override
  State<OnboardingView> createState() => _OnboardingViewState();
}

class _OnboardingViewState extends State<OnboardingView> {
  OnboardingBloc bloc = OnboardingBloc();
  late PageController _controller;
  @override
  void initState() {
    bloc.initState();
    super.initState();
    _controller = PageController(initialPage: 0);
  }

  @override
  void dispose() {
    bloc.dispose();
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (BuildContext context) => bloc,
      child: BlocListener<OnboardingBloc, OnboardingState>(
        listener: (context, state) {},
        child: BlocBuilder<OnboardingBloc, OnboardingState>(
          builder: (context, state) {
            final bloc = context.read<OnboardingBloc>();

            return buildView(
              context,
              bloc,
              state,
            );
          },
        ),
      ),
    );
  }

  buildView(
    BuildContext context,
    OnboardingBloc bloc,
    OnboardingState state,
  ) {
    return SafeArea(
      child: Scaffold(
        body: Container(
          padding: const EdgeInsets.only(
            top: 16,
            left: 16,
            right: 16,
            bottom: 40,
          ).w,
          child: Column(
            children: [
              Expanded(
                child: PageView.builder(
                  controller: _controller,
                  itemCount: state.onBoardingMessage.length,
                  onPageChanged: (value) => bloc.add(
                    OnboardingOnPageChangeEvent(index: value),
                  ),
                  itemBuilder: (_, i) {
                    final item = state.onBoardingMessage[i];
                    return OnBoardingMessageWidget(
                      imageUrl: item['image']!,
                      title: item['title']!,
                      message: item['message']!,
                    );
                  },
                ),
              ),
              OnboardingDotIndicatorWidget(
                totalData: state.onBoardingMessage.length,
                selectedIndex: state.selectedIndex,
              ),
              SizedBox(
                height: 24.0.h,
              ),
              ButtonPrimary(
                label: state.selectedIndex == state.onBoardingMessage.length - 1
                    ? 'Continue'
                    : 'Next',
                onPressed: () async {
                  if (state.selectedIndex ==
                      state.onBoardingMessage.length - 1) {
                    SharedPreferences pref =
                        await SharedPreferences.getInstance();
                    await pref.setBool('alreadyVisited', true);

                    if (!context.mounted) return;

                    Navigator.pushReplacement(
                      context,
                      MaterialPageRoute(
                        builder: (context) => const LoginView(),
                      ),
                    );
                  }

                  _controller.nextPage(
                    duration: const Duration(milliseconds: 100),
                    curve: Curves.bounceIn,
                  );
                },
              )
            ],
          ),
        ),
      ),
    );
  }
}
