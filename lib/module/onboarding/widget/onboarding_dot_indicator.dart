import 'package:bloc_pokedex_app/shared/theme/theme_config.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class OnboardingDotIndicatorWidget extends StatelessWidget {
  final int totalData;
  final int selectedIndex;

  const OnboardingDotIndicatorWidget(
      {super.key, required this.totalData, required this.selectedIndex});

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: List.generate(
          totalData,
          (index) => Container(
            height: 9.h,
            width: (selectedIndex == index ? 25 : 10).w,
            margin: const EdgeInsets.only(right: 8).w,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(11).r,
              color: selectedIndex == index ? primaryColor : disabledColor,
            ),
          ),
        ),
      ),
    );
  }
}
