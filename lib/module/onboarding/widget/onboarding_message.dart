import 'package:bloc_pokedex_app/shared/theme/theme_config.dart';
import 'package:bloc_pokedex_app/shared/widget/typography/pk_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class OnBoardingMessageWidget extends StatelessWidget {
  final String imageUrl;
  final String title;
  final String message;

  const OnBoardingMessageWidget({
    super.key,
    required this.imageUrl,
    required this.title,
    required this.message,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 10).w,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          const Spacer(),
          Image.asset(
            imageUrl,
          ),
          SizedBox(
            height: 45.0.h,
          ),
          PKText(
            label: title,
            fontSize: 26,
            fontWeight: FontWeight.w500,
          ),
          SizedBox(
            height: 16.0.h,
          ),
          PKText(
            label: message,
            fontSize: 14,
            fontWeight: FontWeight.w400,
            color: grayColor,
          ),
          const Spacer(),
        ],
      ),
    );
  }
}
