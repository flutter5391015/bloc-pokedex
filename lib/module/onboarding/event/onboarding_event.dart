abstract class OnboardingEvent {}

class OnboardingOnLoadEvent extends OnboardingEvent {}

class OnboardingOnPageChangeEvent extends OnboardingEvent {
  int index;
  OnboardingOnPageChangeEvent({required this.index});
}
