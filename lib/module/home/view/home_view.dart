import 'package:bloc_pokedex_app/core.dart';
import 'package:bloc_pokedex_app/module/home/widget/home_bottom_navbar.dart';

import 'package:bloc_pokedex_app/module/pokedex/widget/pokedex_textfield_widget.dart';
import 'package:bloc_pokedex_app/utils/focus_helper.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import '../bloc/home_bloc.dart';
import '../event/home_event.dart';
import '../state/home_state.dart';

class HomeView extends StatefulWidget {
  const HomeView({Key? key}) : super(key: key);

  @override
  State<HomeView> createState() => _HomeViewState();
}

class _HomeViewState extends State<HomeView> {
  HomeBloc bloc = HomeBloc();
  late FocusNode _searchFocusNode;

  @override
  void initState() {
    bloc.initState();
    _searchFocusNode = FocusNode();
    super.initState();
  }

  @override
  void dispose() {
    bloc.dispose();
    _searchFocusNode.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (BuildContext context) => bloc,
      child: BlocListener<HomeBloc, HomeState>(
        listener: (context, state) {},
        child: BlocBuilder<HomeBloc, HomeState>(
          builder: (context, state) {
            final bloc = context.read<HomeBloc>();
            return buildView(context, bloc, state);
          },
        ),
      ),
    );
  }

  buildView(
    BuildContext context,
    HomeBloc bloc,
    HomeState state,
  ) {
    return GestureDetector(
      onTap: () => FocusHelper.unfocus(context),
      child: Scaffold(
        body: SafeArea(
          child: PageView.builder(
            itemCount: state.menuList.length,
            itemBuilder: (context, index) {
              return const PokedexView();
            },
          ),
        ),
        bottomNavigationBar: HomeBottomNavbar(
          menuList: state.menuList,
          selectedIndex: state.selectedIndex,
          onChanged: (value) => bloc.add(
            HomeOnSelectedIndexEvent(selectedIndex: value),
          ),
        ),
      ),
    );
  }
}
