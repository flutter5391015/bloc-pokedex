abstract class HomeEvent {}

class HomeOnInitEvent extends HomeEvent {}

class HomeOnSelectedIndexEvent extends HomeEvent {
  final int selectedIndex;

  HomeOnSelectedIndexEvent({
    required this.selectedIndex,
  });
}
