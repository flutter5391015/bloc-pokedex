import 'package:flutter_bloc/flutter_bloc.dart';
import '../event/home_event.dart';
import '../state/home_state.dart';

mixin _BlocLifecycle {
  void initState() {}
  void dispose() {}
}

class HomeBloc extends Bloc<HomeEvent, HomeState> with _BlocLifecycle {
  HomeBloc() : super(HomeState()) {
    on<HomeOnInitEvent>((event, emit) {
      state.menuList = [
        {
          "id": 0,
          "label": 'Pokedéx',
          "default_icon": "assets/images/pokedex-disabled.svg",
          "active_icon": "assets/images/pokedex-active.svg",
        },
        {
          "id": 1,
          "label": 'Regions',
          "default_icon": "assets/images/region-disabled.svg",
          "active_icon": "assets/images/region-active.svg",
        },
        {
          "id": 2,
          "label": 'Favorite',
          "default_icon": "assets/images/fav-disabled.svg",
          "active_icon": "assets/images/fav-active.svg",
        },
        {
          "id": 3,
          "label": 'Profile',
          "default_icon": "assets/images/profile-disabled.svg",
          "active_icon": "assets/images/profile-active.svg",
        },
      ];

      state.selectedIndex = 0;
      emit(state.copyWith());
    });

    on<HomeOnSelectedIndexEvent>((event, emit) {
      state.selectedIndex = event.selectedIndex;
      emit(state.copyWith());
    });
    initState();
  }

  @override
  void initState() {
    //initState event
    add(HomeOnInitEvent());
    super.initState();
  }

  @override
  void dispose() {
    //dispose event
    super.dispose();
  }

  @override
  Future<void> close() {
    dispose();
    return super.close();
  }
}
