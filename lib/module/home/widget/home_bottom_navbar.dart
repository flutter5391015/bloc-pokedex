import 'package:bloc_pokedex_app/shared/theme/theme_config.dart';
import 'package:bloc_pokedex_app/shared/widget/typography/pk_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';

class HomeBottomNavbar extends StatelessWidget {
  final List<Map<String, dynamic>> menuList;
  final int selectedIndex;
  final Function(int) onChanged;

  const HomeBottomNavbar({
    super.key,
    required this.menuList,
    required this.selectedIndex,
    required this.onChanged,
  });

  @override
  Widget build(BuildContext context) {
    double bottomSafeAreaHeight = MediaQuery.of(context).padding.bottom;

    return Container(
      height: bottomSafeAreaHeight + 72.h,
      padding: const EdgeInsets.symmetric(
        horizontal: 32,
        vertical: 8,
      ).w,
      decoration: BoxDecoration(
        color: Colors.white,
        boxShadow: [
          BoxShadow(
            color: Colors.grey.withOpacity(0.5),
            spreadRadius: 1,
            blurRadius: 5,
            offset: const Offset(0, 3), // atur posisi bayangan
          ),
        ],
      ),
      child: Row(
        children: List.generate(menuList.length, (index) {
          var item = menuList[index];

          return GestureDetector(
            onTap: () => onChanged(index),
            child: Container(
              width: 56.w,
              height: 56.h,
              margin: index == menuList.length - 1
                  ? EdgeInsets.only(
                      bottom: bottomSafeAreaHeight,
                    ).w
                  : EdgeInsets.only(
                      right: 24,
                      bottom: bottomSafeAreaHeight,
                    ).w,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Expanded(
                    child: SvgPicture.asset(
                      selectedIndex == index
                          ? item['active_icon']
                          : item['default_icon'],
                    ),
                  ),
                  if (selectedIndex == index) ...[
                    PKText(
                      label: item['label'],
                      fontSize: 12,
                      color: primaryColor,
                    ),
                  ]
                ],
              ),
            ),
          );
        }),
      ),
    );
  }
}
