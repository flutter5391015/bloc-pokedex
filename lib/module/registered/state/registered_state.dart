import 'package:freezed_annotation/freezed_annotation.dart';
part 'registered_state.freezed.dart';

@unfreezed
class RegisteredState with _$RegisteredState {
  factory RegisteredState({
    @Default(0) int counter,
  }) = _RegisteredState;
}
