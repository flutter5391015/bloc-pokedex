// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'registered_state.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$RegisteredState {
  int get counter => throw _privateConstructorUsedError;
  set counter(int value) => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $RegisteredStateCopyWith<RegisteredState> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $RegisteredStateCopyWith<$Res> {
  factory $RegisteredStateCopyWith(
          RegisteredState value, $Res Function(RegisteredState) then) =
      _$RegisteredStateCopyWithImpl<$Res, RegisteredState>;
  @useResult
  $Res call({int counter});
}

/// @nodoc
class _$RegisteredStateCopyWithImpl<$Res, $Val extends RegisteredState>
    implements $RegisteredStateCopyWith<$Res> {
  _$RegisteredStateCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? counter = null,
  }) {
    return _then(_value.copyWith(
      counter: null == counter
          ? _value.counter
          : counter // ignore: cast_nullable_to_non_nullable
              as int,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$_RegisteredStateCopyWith<$Res>
    implements $RegisteredStateCopyWith<$Res> {
  factory _$$_RegisteredStateCopyWith(
          _$_RegisteredState value, $Res Function(_$_RegisteredState) then) =
      __$$_RegisteredStateCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({int counter});
}

/// @nodoc
class __$$_RegisteredStateCopyWithImpl<$Res>
    extends _$RegisteredStateCopyWithImpl<$Res, _$_RegisteredState>
    implements _$$_RegisteredStateCopyWith<$Res> {
  __$$_RegisteredStateCopyWithImpl(
      _$_RegisteredState _value, $Res Function(_$_RegisteredState) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? counter = null,
  }) {
    return _then(_$_RegisteredState(
      counter: null == counter
          ? _value.counter
          : counter // ignore: cast_nullable_to_non_nullable
              as int,
    ));
  }
}

/// @nodoc

class _$_RegisteredState implements _RegisteredState {
  _$_RegisteredState({this.counter = 0});

  @override
  @JsonKey()
  int counter;

  @override
  String toString() {
    return 'RegisteredState(counter: $counter)';
  }

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_RegisteredStateCopyWith<_$_RegisteredState> get copyWith =>
      __$$_RegisteredStateCopyWithImpl<_$_RegisteredState>(this, _$identity);
}

abstract class _RegisteredState implements RegisteredState {
  factory _RegisteredState({int counter}) = _$_RegisteredState;

  @override
  int get counter;
  set counter(int value);
  @override
  @JsonKey(ignore: true)
  _$$_RegisteredStateCopyWith<_$_RegisteredState> get copyWith =>
      throw _privateConstructorUsedError;
}
