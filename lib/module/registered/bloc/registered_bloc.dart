import 'package:flutter_bloc/flutter_bloc.dart';
import '../event/registered_event.dart';
import '../state/registered_state.dart';

mixin _BlocLifecycle {
  void initState() {}
  void dispose() {}
}

class RegisteredBloc extends Bloc<RegisteredEvent, RegisteredState>
    with _BlocLifecycle {
  RegisteredBloc() : super(RegisteredState()) {
    on<RegisteredIncrementEvent>((event, emit) {
      state.counter++;
      emit(state.copyWith());
    });
  }

  @override
  void initState() {
    //initState event
    super.initState();
  }

  @override
  void dispose() {
    //dispose event
    super.dispose();
  }

  @override
  Future<void> close() {
    dispose();
    return super.close();
  }
}
