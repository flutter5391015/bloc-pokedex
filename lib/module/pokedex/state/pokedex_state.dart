import 'package:freezed_annotation/freezed_annotation.dart';
part 'pokedex_state.freezed.dart';

@unfreezed
class PokedexState with _$PokedexState {
  factory PokedexState({
    @Default(0) int counter,
  }) = _PokedexState;
}
