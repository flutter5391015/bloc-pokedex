import 'package:bloc_pokedex_app/module/pokedex/widget/pokedex_filter_widget.dart';
import 'package:bloc_pokedex_app/module/pokedex/widget/pokedex_textfield_widget.dart';
import 'package:bloc_pokedex_app/shared/theme/theme_config.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import '../bloc/pokedex_bloc.dart';
import '../event/pokedex_event.dart';
import '../state/pokedex_state.dart';

class PokedexView extends StatefulWidget {
  const PokedexView({Key? key}) : super(key: key);

  @override
  State<PokedexView> createState() => _PokedexViewState();
}

class _PokedexViewState extends State<PokedexView> {
  PokedexBloc bloc = PokedexBloc();
  late FocusNode _searchFocusNode;

  @override
  void initState() {
    bloc.initState();
    _searchFocusNode = FocusNode();
    super.initState();
  }

  @override
  void dispose() {
    bloc.dispose();
    _searchFocusNode.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (BuildContext context) => bloc,
      child: BlocListener<PokedexBloc, PokedexState>(
        listener: (context, state) {},
        child: BlocBuilder<PokedexBloc, PokedexState>(
          builder: (context, state) {
            final bloc = context.read<PokedexBloc>();
            return buildView(context, bloc, state);
          },
        ),
      ),
    );
  }

  buildView(
    BuildContext context,
    PokedexBloc bloc,
    PokedexState state,
  ) {
    return Column(children: [
      Container(
        decoration: BoxDecoration(
          color: Colors.white,
          boxShadow: [
            BoxShadow(
              color: Colors.grey.withOpacity(0.5),
              spreadRadius: .1,
              blurRadius: .1,
              offset: const Offset(0, .5), // atur posisi bayangan
            ),
          ],
        ),
        child: Container(
          margin: const EdgeInsets.symmetric(vertical: 20, horizontal: 16),
          child: PokedexSearchTextWidget(
            onChanged: (value) => print(value),
            focusNode: _searchFocusNode,
            hintText: 'Search Pokemon...',
          ),
        ),
      ),
      Container(
        padding: const EdgeInsets.only(top: 16, left: 16, right: 16).w,
        child: SizedBox(
          width: MediaQuery.of(context).size.width,
          height: 42.h,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              PokedexFilterWidget(
                bgColor: defaultTypeColor,
                label: 'All kinds',
                onTap: () => print('asd'),
              ),
              SizedBox(
                width: 16.0.w,
              ),
              PokedexFilterWidget(
                bgColor: defaultTypeColor,
                label: 'lowest number',
                onTap: () => print('asd'),
              ),
            ],
          ),
        ),
      )
    ]);
  }
}
