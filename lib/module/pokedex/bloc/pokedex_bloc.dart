import 'package:flutter_bloc/flutter_bloc.dart';
import '../event/pokedex_event.dart';
import '../state/pokedex_state.dart';

mixin _BlocLifecycle {
  void initState() {}
  void dispose() {}
}

class PokedexBloc extends Bloc<PokedexEvent, PokedexState> with _BlocLifecycle {
  PokedexBloc() : super(PokedexState()) {
    on<PokedexIncrementEvent>((event, emit) {
      state.counter++;
      emit(state.copyWith());
    });
  }

  @override
  void initState() {
    //initState event
    super.initState();
  }

  @override
  void dispose() {
    //dispose event
    super.dispose();
  }

  @override
  Future<void> close() {
    dispose();
    return super.close();
  }
}
