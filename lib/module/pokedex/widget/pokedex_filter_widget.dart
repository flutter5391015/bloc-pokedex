import 'package:bloc_pokedex_app/shared/theme/theme_config.dart';
import 'package:bloc_pokedex_app/shared/widget/typography/pk_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class PokedexFilterWidget extends StatelessWidget {
  final Color bgColor;
  final String label;
  final void Function() onTap;
  const PokedexFilterWidget({
    super.key,
    required this.bgColor,
    required this.label,
    required this.onTap,
  });

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Container(
        height: MediaQuery.of(context).size.height,
        padding: const EdgeInsets.symmetric(vertical: 9, horizontal: 18).w,
        decoration: BoxDecoration(
          color: bgColor,
          borderRadius: BorderRadius.circular(30).r,
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            PKText(
              label: label,
              fontSize: 14,
              fontWeight: FontWeight.w600,
              color: whiteTextColor,
            ),
            InkWell(
              enableFeedback: false,
              splashColor: Colors.transparent,
              onTap: () => onTap,
              child: Container(
                padding: const EdgeInsets.all(0),
                margin: const EdgeInsets.all(0),
                child: const Icon(
                  Icons.keyboard_arrow_down,
                  color: whiteTextColor,
                  size: 20,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
