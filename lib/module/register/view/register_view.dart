import 'package:bloc_pokedex_app/core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class RegisterView extends StatefulWidget {
  const RegisterView({Key? key}) : super(key: key);

  @override
  State<RegisterView> createState() => _RegisterViewState();
}

class _RegisterViewState extends State<RegisterView> {
  RegisterBloc bloc = RegisterBloc();

  @override
  void initState() {
    bloc.initState();
    super.initState();
  }

  @override
  void dispose() {
    bloc.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (BuildContext context) => bloc,
      child: BlocListener<RegisterBloc, RegisterState>(
        listener: (context, state) {},
        child: BlocBuilder<RegisterBloc, RegisterState>(
          builder: (context, state) {
            final bloc = context.read<RegisterBloc>();
            return buildView(context, bloc, state);
          },
        ),
      ),
    );
  }

  buildView(
    BuildContext context,
    RegisterBloc bloc,
    RegisterState state,
  ) {
    return Scaffold(
      appBar: AppBar(
        title: const AppBarText(label: 'Create an account'),
      ),
      body: Container(
        padding: const EdgeInsets.only(left: 16, right: 16, bottom: 40).w,
        child: SizedBox(
          width: MediaQuery.of(context).size.width,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            mainAxisSize: MainAxisSize.min,
            children: [
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Image.asset(
                      "assets/images/register.png",
                    ),
                    SizedBox(
                      height: 23.0.h,
                    ),
                    const PKText(
                      label: 'Not long to explore this world!',
                      fontWeight: FontWeight.w500,
                      fontSize: 26,
                    ),
                    SizedBox(
                      height: 16.0.h,
                    ),
                    const PKText(
                      label: 'How do you want to connect?',
                      fontWeight: FontWeight.w400,
                      fontSize: 14,
                      color: grayColor,
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: 32.0.h,
              ),
              ButtonOutlineIcon(
                icon: 'assets/images/google.svg',
                label: 'Continue with Google',
                onPressed: () => print('asd'),
              ),
              SizedBox(
                height: 12.0.h,
              ),
              ButtonPrimary(
                label: 'Continue with an email',
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => const FormRegisterEmailView(),
                    ),
                  );
                },
              ),
            ],
          ),
        ),
      ),
    );
  }
}
